package ru.nsu.fit.kazak.network.lab5;

import java.io.IOException;
import java.net.*;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static ru.nsu.fit.kazak.network.lab5.KeyInfo.Type.*;

public class Socks5Server implements Runnable {
    private static final Logger logger = Logger.getLogger(Socks5Server.class.getName());
    private ServerSocketChannel server;
    private Selector selector;
    private int port;

    public Socks5Server(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        try {
            server = java.nio.channels.ServerSocketChannel.open();
            selector = Selector.open();
            server.bind(new InetSocketAddress(port));
            server.configureBlocking(false);
            server.register(selector, server.validOps(), new KeyInfo(SERVER));
            DnsRequester.getInstance().init(selector);
            logger.info("Start listening port " + port);
            while (true) {
                try {
                    selector.select();
                    Set<SelectionKey> selectedKeys = selector.selectedKeys();
                    Iterator<SelectionKey> iter = selectedKeys.iterator();
                    while (iter.hasNext()) {
                        SelectionKey key = iter.next();
                        iter.remove();
                        if (key.isValid()) {
                            switch (((KeyInfo) key.attachment()).getType()) {
                                case SERVER: ServerHandler.handle(key, (KeyInfo) key.attachment());
                                    break;
                                case CLIENT: ClientHandler.handle(key, (KeyInfo) key.attachment());
                                    break;
                                case HOST: HostHandler.handle(key, (KeyInfo) key.attachment());
                                    break;
                                case DNS: DnsRequester.getInstance().handleRequest();
                            }
                        }
                    }
                } catch (IOException ex) {
                    logger.log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        } finally {
            try {
                server.close();
                selector.close();
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Port number expected =(");
            }
        }
    }
}
