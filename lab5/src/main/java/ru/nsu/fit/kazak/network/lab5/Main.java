package ru.nsu.fit.kazak.network.lab5;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        Logger.getGlobal().setLevel(Level.OFF);
        logger.info("Start socks5 server");
        if (args.length  <  1) {
            logger.log(Level.SEVERE, "Port number expected =(");
            return;
        }
        try {
            Socks5Server server = new Socks5Server(Integer.parseInt(args[0]));
            server.run();
        } catch (NumberFormatException ex) {
            logger.log(Level.SEVERE, "Wrong port format =(", ex);
        }
    }
}
