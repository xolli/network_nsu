package ru.nsu.fit.kazak.network.lab5;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import java.util.logging.Logger;

import static java.nio.channels.SelectionKey.*;
import static ru.nsu.fit.kazak.network.lab5.KeyInfo.State.*;

public class ClientHandler {
    private static final Logger logger = Logger.getLogger(ClientHandler.class.getName());

    static public void handle(SelectionKey key, KeyInfo info) throws IOException {
        switch (info.getState()) {
            case NOT_INIT: initClient(key, info);
            break;
            case WAIT_INIT_RESULT: sendInitResult(key, info);
            break;
            case FIRST_READ: readHelloMessage(key, info);
            break;
            case WAIT_HELLO: sendHelloToClient(key, info);
            break;
            case INIT: handleInitClient(key, info);
        }
    }

    private static void handleInitClient(SelectionKey key, KeyInfo info) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        if (key.isReadable()) {
            ByteBuffer hostWritableBuffer = getHostWritableBuffer(info.getHostKey());
            int countRead = channel.read(hostWritableBuffer);
            if (countRead < 0) {
                closeAllConnections(key, info);
                return;
            }
//            logger.info(countRead + " bytes read from client =)");
            setWriteStateToHost(info.getHostKey());
        } else if (key.isWritable()) {
            info.getWritableBuffer().flip();
            int countWrite = channel.write(info.getWritableBuffer());
            if (countWrite < 0) {
                closeAllConnections(key, info);
                return;
            }
//            logger.info(countWrite + " write to client");
            if (info.getWritableBuffer().remaining() == 0) {
                info.getWritableBuffer().clear();
                key.interestOps(SelectionKey.OP_READ);
                if (!info.getHostKey().isValid()) {
                    closeAllConnections(key, info);
                }
            }
            else {
                info.getWritableBuffer().compact();
            }
        }
    }

    private static void closeAllConnections(SelectionKey key, KeyInfo info) throws IOException {
        key.cancel();
        key.channel().close();
        if (info.getHostKey() != null) {
            info.getHostKey().cancel();
            info.getHostKey().channel().close();
        }
    }

    private static void setWriteStateToHost(SelectionKey hostKey) {
        hostKey.interestOps(hostKey.interestOps() | OP_WRITE);
    }

    private static ByteBuffer getHostWritableBuffer(SelectionKey hostKey) {
        return ((KeyInfo)hostKey.attachment()).getWritableBuffer();
    }

    private static void sendHelloToClient(SelectionKey key, KeyInfo info) throws IOException {
        if (!key.isWritable()) {
            return;
        }
        ByteBuffer answer = ByteBuffer.allocate(4 + info.getHostAddressPort().length);
        answer.put(0, (byte) 0x05); // version
        answer.put(2, (byte) 0x00); // reserve
        answer.put(3, info.getAddressType()); // address type
        for (int i = 0; i < info.getHostAddressPort().length; ++i) {
            answer.put(4 + i, info.getHostAddressPort()[i]);
        }
        answer.put(1, (byte) 0x00); // good answer code
        ((SocketChannel) key.channel()).write(answer);
        info.setState(INIT);
        key.interestOps(OP_READ);
    }

    private static void readHelloMessage(SelectionKey key, KeyInfo info) throws IOException {
        if (!key.isReadable()) {
            return;
        }
        SocketChannel channel = (SocketChannel) key.channel();
        ByteBuffer buffer = ByteBuffer.allocate(256);
        int countRead = channel.read(buffer);
        if (countRead == -1) {
            logger.severe("cannot read hello message =(");
            closeAllConnections(key, info);
            return;
        }
        if (buffer.get(0) == 0x5 && buffer.get(1) == 0x01) {
            switch (buffer.get(3)) {
                case 0x01: createTcpConnection(key, info, buffer, countRead);
                break;
                case 0x03: createDnsRequest(key, info, buffer);
            }
        }
        else {
            logger.severe("Operation doesn't support or client's socket version is wrong =(");
            closeAllConnections(key, info);
            return;
        }
        logger.info("readHelloMessage successfully");
    }

    private static void createDnsRequest(SelectionKey key, KeyInfo info, ByteBuffer buffer) {
        int addrLen = buffer.get(4);
        String host = new String(Arrays.copyOfRange(buffer.array(), 5, 5 + addrLen));
        info.setHostAddressPort(Arrays.copyOfRange(buffer.array(), 4, 5 + addrLen + 2), (byte) 0x03);
        DnsRequester.getInstance().addRequest(info, host);
        info.setState(WAIT_HELLO);
        key.interestOps(0);
    }

    private static void createTcpConnection(SelectionKey key, KeyInfo info, ByteBuffer buffer, int countRead) throws IOException {
        byte[] hostAddressBytes = Arrays.copyOfRange(buffer.array(), 4, 8);
        InetAddress hostAddress = InetAddress.getByAddress(hostAddressBytes);
        logger.info("Host ip: " + hostAddress.getHostAddress());
        int hostPort = ByteBuffer.wrap(Arrays.copyOfRange(buffer.array(), countRead - 2, countRead)).getShort();
        logger.info("Host port: " + hostPort);

        SocketChannel hostChannel = SocketChannel.open();
        hostChannel.configureBlocking(false);
        hostChannel.connect(new InetSocketAddress(hostAddress, hostPort));
        logger.info("Start connecting to host");
        SelectionKey hostKey = hostChannel.register(key.selector(), OP_CONNECT, new KeyInfo(KeyInfo.Type.HOST, CONNECT, key, null));
        info.setState(WAIT_HELLO);
        info.setHostKey(hostKey);
        info.setHostAddressPort(Arrays.copyOfRange(buffer.array(), 4, 10), (byte) 0x01);
        key.interestOps(0);
    }

    private static void initClient(SelectionKey key, KeyInfo info) throws IOException {
        if (!key.isReadable()) {
            return;
        }
        info.setClientKey(key);
        SocketChannel channel = (SocketChannel) key.channel();
        ByteBuffer buffer = ByteBuffer.allocate(256);
        int countRead = channel.read(buffer);
        if (countRead == -1) {
            logger.severe("cannot read init message =(");
            closeAllConnections(key, info);
            return;
        }
        if (buffer.get(0) == 0x5) {
            key.interestOps(OP_WRITE);
            info.setState(WAIT_INIT_RESULT);
        }
        else {
            logger.severe("Operation doesn't support or client's socket version is wrong =(");
            return;
        }
        logger.info("handleSocksInit");
    }

    private static void sendInitResult(SelectionKey key, KeyInfo info) throws IOException {
        if (!key.isWritable()) {
            return;
        }
        SocketChannel channel = (SocketChannel) key.channel();
        ByteBuffer answer = ByteBuffer.allocate(2);
        answer.put(0, (byte) 0x5);
        answer.put(1, (byte) 0x0);
        channel.write(answer);
        logger.info("Init connection");
        key.interestOps(OP_READ);
        info.setState(FIRST_READ);
    }
}
