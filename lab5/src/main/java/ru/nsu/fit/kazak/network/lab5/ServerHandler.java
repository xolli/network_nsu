package ru.nsu.fit.kazak.network.lab5;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.logging.Logger;

public class ServerHandler {
    private static final Logger logger = Logger.getLogger(ServerHandler.class.getName());

    static public void handle(SelectionKey key, KeyInfo info) throws IOException {
        ServerSocketChannel serverChannel = (ServerSocketChannel) key.channel();
        SocketChannel client = serverChannel.accept();
        if (client == null) {
            logger.severe("client is null =(");
            return;
        }
        logger.info("accept connection on port " + serverChannel.socket().getLocalPort());
        client.configureBlocking(false);
        KeyInfo clientInfo = new KeyInfo(KeyInfo.Type.CLIENT);
        clientInfo.setState(KeyInfo.State.NOT_INIT);
        client.register(key.selector(), SelectionKey.OP_READ, clientInfo);
    }
}
