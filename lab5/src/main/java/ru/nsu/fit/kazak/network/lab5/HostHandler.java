package ru.nsu.fit.kazak.network.lab5;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.logging.Logger;

import static java.nio.channels.SelectionKey.*;
import static ru.nsu.fit.kazak.network.lab5.KeyInfo.State.INIT;

public class HostHandler {
    private static final Logger logger = Logger.getLogger(HostHandler.class.getName());

    static public void handle(SelectionKey key, KeyInfo info) throws IOException {
        switch (info.getState()) {
            case CONNECT: connectToHost(key, info);
            break;
            case INIT: handleInitHost(key, info);
        }
    }

    private static void handleInitHost(SelectionKey key, KeyInfo info) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        if (key.isWritable()) {
            info.getWritableBuffer().flip();
            int countRead = channel.write(info.getWritableBuffer());
            if (countRead < 0) {
                logger.severe("close all connection.");
                closeAllConnections(key, info);
                return;
            }
//            logger.info(countRead + " bytes sent to host =)");
            if (info.getWritableBuffer().remaining() == 0) {
                info.getWritableBuffer().clear();
                key.interestOps(SelectionKey.OP_READ);
            }
            else {
                info.getWritableBuffer().compact();
            }
        } else if (key.isReadable()) {
            ByteBuffer clientWritableBuffer = getClientWritableBuffer(info.getClientKey());
            int countRead = channel.read(clientWritableBuffer);
            if (countRead < 0) {
                closeAllConnections(key, info);
                return;
            }
//            logger.info(countRead + " read from host =)");
            setWriteStateToClient(info.getClientKey());
        }
    }

    private static void setWriteStateToClient(SelectionKey clientKey) {
        clientKey.interestOps(clientKey.interestOps() | OP_WRITE);
    }

    private static ByteBuffer getClientWritableBuffer(SelectionKey clientKey) {
        return ((KeyInfo)clientKey.attachment()).getWritableBuffer();
    }

    private static void connectToHost(SelectionKey key, KeyInfo info) throws IOException {
        SocketChannel hostChannel = (SocketChannel) key.channel();
        if (!info.getClientKey().isValid()) {
            key.cancel();
            hostChannel.close();
            logger.severe("Client is invalid =(");
            return;
        }
        if (!key.isConnectable()) {
            logger.severe("Host key is not connectable 0_o");
            return;
        }
        hostChannel.finishConnect();
        key.interestOps(OP_READ);
        SelectionKey clientKey = info.getClientKey();
        clientKey.interestOps(OP_WRITE);
        info.setState(INIT);
        logger.info("Connect to host " + info + "!");
    }

    private static void closeAllConnections(SelectionKey key, KeyInfo info) throws IOException {
        key.cancel();
        key.channel().close();
        info.getClientKey().cancel();
        info.getClientKey().channel().close();
    }
}
