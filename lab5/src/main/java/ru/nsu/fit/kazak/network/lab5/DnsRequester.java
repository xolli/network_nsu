package ru.nsu.fit.kazak.network.lab5;

import org.xbill.DNS.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.channels.SelectionKey.OP_CONNECT;
import static ru.nsu.fit.kazak.network.lab5.KeyInfo.State.CONNECT;

public class DnsRequester {
    private static final Logger logger = Logger.getLogger(DnsRequester.class.getName());

    private DatagramChannel channel;
    private Queue<Request> requests;
    private HashMap<Name, KeyInfo> responses;
    private SelectionKey key;

    private DnsRequester() {}

    private static class DnsRequesterHolder {
        private static final DnsRequester instance = new DnsRequester();
    }

    public static DnsRequester getInstance() {
        return DnsRequesterHolder.instance;
    }

    public void init(Selector selector) throws IOException {
        InetSocketAddress dnsServer = ResolverConfig.getCurrentConfig().server();
        channel = DatagramChannel.open();
        channel.socket().connect(dnsServer);
        channel.configureBlocking(false);
        key = channel.register(selector, 0, new KeyInfo(KeyInfo.Type.DNS));
        requests = new ConcurrentLinkedDeque<>();
        responses = new HashMap<>();
    }

    public void addRequest(KeyInfo clientInfo, String address) {
        requests.add(new Request(clientInfo, address));
        key.interestOps(key.interestOps() | SelectionKey.OP_WRITE);
        logger.info("Create new DNS request =)");
    }

    public void handleRequest() throws IOException {
        if (key.isWritable()) {
            sendRequest();
        }
        if (key.isReadable()) {
            readAnswer();
        }
    }

    private void readAnswer() throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(512);
        int countRead = channel.read(buffer);
        if (countRead <= 0) {
            return;
        }
        Message message = new Message(buffer.array());
        Name hostName = message.getQuestion().getName();
        List<Record> answers = message.getSection(Section.ANSWER);
        logger.info("Reading DNS answer for " + hostName);
        if (!responses.containsKey(hostName)) {
            logger.severe("Cannot find hostname =(");
            return;
        }
        KeyInfo clientInfo = responses.get(hostName);
        for (Record answer : answers) {
            if (answer.getType() == Type.A) {
                InetAddress inetAddress = ((ARecord) answer).getAddress();
                setHostAddress(clientInfo, inetAddress);
                break;
            }
        }
    }

    private void setHostAddress(KeyInfo clientInfo, InetAddress host) throws IOException {
        if (host == null || !clientInfo.getClientKey().isValid()) {
            logger.info("Cannot find host ip address or client cancel connection =(");
            return;
        }
        SocketChannel hostChannel = SocketChannel.open();
        hostChannel.configureBlocking(false);
        hostChannel.connect(new InetSocketAddress(host, clientInfo.getHostPort()));
        logger.info("Start connecting to host " + host.getHostName());
        SelectionKey hostKey = hostChannel.register(clientInfo.getClientKey().selector(), OP_CONNECT, new KeyInfo(KeyInfo.Type.HOST, CONNECT, clientInfo.getClientKey(), null));
        clientInfo.setHostKey(hostKey);
        logger.info("Find ip from host: " + host.getHostAddress());
    }

    private void sendRequest() throws IOException {
        Message message = new Message();
        Header header = new Header();
        header.setFlag(Flags.RD);
        message.setHeader(header);
        System.out.println("requests: " + requests);
        Request request = requests.remove();
        try {
            message.addRecord(Record.newRecord(Name.fromString(request.address, Name.root), Type.A, DClass.IN), Section.QUESTION);
            responses.put(Name.fromString(request.address, Name.root), request.clientInfo);
        }
        catch (TextParseException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            return;
        }
        ByteBuffer buffer = ByteBuffer.allocate(512);
        buffer.put(message.toWire());
        buffer.flip();
        channel.write(buffer);
        key.interestOps(key.interestOps() | SelectionKey.OP_READ);
        if (requests.isEmpty()) {
            key.interestOps(SelectionKey.OP_READ);
        }
        logger.info("DNS request send " + request.address);
    }

    private static class Request {
        KeyInfo clientInfo;
        String address;

        private Request(KeyInfo clientInfo, String address) {
            this.clientInfo = clientInfo;
            this.address = address;
        }

        @Override
        public String toString() {
            return address;
        }
    }
}
