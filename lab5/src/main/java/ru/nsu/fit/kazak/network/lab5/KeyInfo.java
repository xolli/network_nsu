package ru.nsu.fit.kazak.network.lab5;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;

public class KeyInfo {
    public enum Type {
        CLIENT,
        HOST,
        SERVER,
        DNS
    }

    public enum State {
        NOT_INIT,
        WAIT_INIT_RESULT,
        FIRST_READ,
        WAIT_HELLO,
        INIT,
        CONNECT
    }

    private Type type;
    private State state;
    private SelectionKey clientKey;
    private SelectionKey hostKey;
    private byte[] hostAddressPort;
    private final ByteBuffer writableBuffer;
    private byte addressType;

    public KeyInfo(Type type) {
        this.type = type;
        this.writableBuffer = ByteBuffer.allocate(16384);
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public KeyInfo(Type type, State state, SelectionKey clientKey, SelectionKey hostKey) {
        this.type = type;
        this.state = state;
        this.clientKey = clientKey;
        this.hostKey = hostKey;
        this.writableBuffer = ByteBuffer.allocate(16384);
    }

    public ByteBuffer getWritableBuffer() {
        return writableBuffer;
    }

    public Type getType() {
        return type;
    }

    public SelectionKey getClientKey() {
        return clientKey;
    }

    public SelectionKey getHostKey() {
        return hostKey;
    }

    public byte[] getHostAddressPort() {
        return hostAddressPort;
    }

    public byte getAddressType() {
        return addressType;
    }

    public void setHostAddressPort(byte[] hostAddressPort, byte addressType) {
        this.hostAddressPort = hostAddressPort;
        this.addressType = addressType;
    }

    public void setHostKey(SelectionKey hostKey) {
        this.hostKey = hostKey;
    }

    public void setClientKey(SelectionKey clientKey) {
        this.clientKey = clientKey;
    }

    //https://stackoverflow.com/questions/7619058/convert-a-byte-array-to-integer-in-java-and-vice-versa
    public int getHostPort() {
        byte[] arr = { hostAddressPort[hostAddressPort.length - 2], hostAddressPort[hostAddressPort.length - 1] };
        ByteBuffer wrapped = ByteBuffer.wrap(arr); // big-endian by default
        return wrapped.getShort();
    }

}
