package ru.nsu.fit.kazak.network.lab3;

public class Pinger implements Runnable {
    private final Neighbours neighbours;
    private final Sender sender;

    public Pinger(Sender sender) {
        this.sender = sender;
        neighbours = Neighbours.getInstance();
    }

    @Override
    public void run() {
        ControllerWork controller = ControllerWork.getInstance();
        try {
            while (controller.canContinueWork()) {
                Thread.sleep(10);
                Packet pingPacket = new Packet(TypePacket.I_AM_ALIVE);
                for (InterlocutorNeighbour neighbour : neighbours.getLifeNeighboursSet()) {
                    sender.sendPacketWithoutConfirm(neighbour, pingPacket);
                }
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}

