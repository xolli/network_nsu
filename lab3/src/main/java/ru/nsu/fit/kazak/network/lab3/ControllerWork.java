package ru.nsu.fit.kazak.network.lab3;

import java.util.concurrent.atomic.AtomicBoolean;

public class ControllerWork {
    private final AtomicBoolean continueWork;

    private ControllerWork() {
        continueWork = new AtomicBoolean(true);
    }

    private static class ControllerWorkHolder {
        private final static ControllerWork instance = new ControllerWork();
    }

    public static ControllerWork getInstance() {
        return ControllerWorkHolder.instance;
    }

    public boolean canContinueWork() {
        return continueWork.get();
    }

    public void endWork() {
        continueWork.set(false);
    }
}
