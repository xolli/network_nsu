package ru.nsu.fit.kazak.network.lab3;

import java.io.Closeable;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Scanner;

public class InterlocutorPeer implements Closeable {
    private final Neighbours neighbours;
    private final String name;
    private Sender sender;
    private ListenerNewMessage listenerNewMessage;
    private ListenerNeighbours listenerNeighbours;
    private DatagramSocket socket;

    private final Thread threadPinger;
    private Thread listenerNewMessageThread;
    private Thread listenerNeighboursThread;
    private final Thread resendThread;

    public InterlocutorPeer(String name, int port, int percentLoss) throws SocketException {
        this.name = name;
        neighbours = Neighbours.getInstance();
        initUDPSocket(port, percentLoss);
        Pinger pinger = new Pinger(sender);
        resendThread = new Thread(new Resender(NotConfirmedPackets.getInstance(), sender));
        resendThread.start();
        threadPinger = new Thread(pinger);
        threadPinger.start();
    }

    public InterlocutorPeer(String name, int port, InterlocutorNeighbour firstNeighbour, int percentLoss) throws SocketException {
        this(name, port, percentLoss);
        neighbours.addNeighbour(firstNeighbour);
        neighbours.setDeputy(firstNeighbour);
        sender.sendNeighbourRequest(firstNeighbour);
    }

    private void initUDPSocket(int port, int percentLoss) throws SocketException {
        socket = new DatagramSocket(port);
        NotConfirmedPackets notConfirmedPackets = NotConfirmedPackets.getInstance();
        sender = new Sender(socket, percentLoss);
        listenerNewMessage = new ListenerNewMessage(this, socket, notConfirmedPackets);
        listenerNeighbours = new ListenerNeighbours(this, sender);
    }

    public void say(String message) {
        for (InterlocutorNeighbour neighbour : neighbours.getLifeNeighboursSet()) {
            sendMessage(message, neighbour);
        }
    }

    private void transferMessage(Packet transferPacket, InterlocutorNeighbour source) {
        for (InterlocutorNeighbour neighbour : neighbours.getLifeNeighboursSet()) {
            if (!neighbour.equals(source)) {
                Packet newPacket = new Packet(transferPacket);
                sender.sendPacket(neighbour, newPacket);
            }
        }
    }

    private void sendMessage (String message, InterlocutorNeighbour destination) {
        sender.sendMessage(destination, name, message);
    }

    public void acceptPacket(Packet newPacket, InterlocutorNeighbour source) {
        if (neighbours.isDied(source)) {
            neighbours.revive(source);
            if (!neighbours.haveDeputy()) {
                neighbours.setDeputy(source);
                sender.sendPacket(source, new Packet(TypePacket.NO_DEPUTY));
            }
        }
        if (newPacket.isTextPacket()) {
            sender.sendConfirmed(source, newPacket.getUUID());
            acceptMessage(newPacket, source);
        } else if (newPacket.isNewNeighbourPacket()) {
            sender.sendConfirmed(source, newPacket.getUUID());
            acceptNewNeighbour(source);
        } else if (newPacket.isLifePacket()) {
            listenerNeighbours.updateActivity(source);
        } else if (newPacket.isDeputyPacket()) {
            sender.sendConfirmed(source, newPacket.getUUID());
            neighbours.addReplace(source, newPacket.getDeputy());
        } else if (newPacket.isNoDeputyPacket()) {
            sender.sendConfirmed(source, newPacket.getUUID());
            neighbours.delNeighbourReplace(source);
        }
    }

    private void acceptNewNeighbour(InterlocutorNeighbour newNeighbour) {
        neighbours.addNeighbour(newNeighbour);
        if (neighbours.haveDeputy() && !neighbours.isDeputy(newNeighbour)) {
            sender.sendPacket(newNeighbour, new Packet(neighbours.getDeputy()));
        } else if (!neighbours.haveDeputy()) {
            sender.sendPacket(newNeighbour, new Packet(TypePacket.NO_DEPUTY));
            neighbours.setDeputy(newNeighbour);
            notifyAllAboutDeputy();
        }
    }

    public void acceptMessage(Packet messagePacket, InterlocutorNeighbour source) {
        System.out.println(messagePacket.getName() + ": " + messagePacket.getText());
        transferMessage(messagePacket, source);
    }

    public void speak() {
        Scanner in = new Scanner(System.in);
        String newLine = in.nextLine();
        while (!newLine.equals("")) {
            say(newLine);
            newLine = in.nextLine();
        }
        in.close();
    }

    public void startListen() {
        listenerNewMessageThread = new Thread(listenerNewMessage);
        listenerNeighboursThread = new Thread(listenerNeighbours);
        listenerNewMessageThread.start();
        listenerNeighboursThread.start();
    }

    public void notifyAllAboutDeputy() {
        if (neighbours.haveDeputy()) {
            for (InterlocutorNeighbour neighbour : neighbours.getLifeNeighboursSet()) {
                if (!neighbour.equals(neighbours.getDeputy())) {
                    System.out.println("Send deputy " + neighbours.getDeputy().getPort() + " to " + neighbour.getPort());
                    sender.sendMyDeputy(neighbour, neighbours.getDeputy());
                } else {
                    System.out.println("Send my deputy " + neighbours.getDeputy().getPort() + " that he is my deputy");
                    sender.sendPacket(neighbour, new Packet(TypePacket.NO_DEPUTY));
                }
            }
        } else {
            for (InterlocutorNeighbour neighbour : neighbours.getLifeNeighboursSet()) {
                System.out.println("Notify about no deputy " + neighbour.getPort());
                sender.sendPacket(neighbour, new Packet(TypePacket.NO_DEPUTY));
            }
        }
    }


    @Override
    public void close() {
        ControllerWork.getInstance().endWork();
        sender.sendPacketWithoutLoss(new InterlocutorNeighbour("127.0.0.1", socket.getLocalPort()), new Packet(TypePacket.END_OF_WORK));
        try {
            listenerNewMessageThread.join();
            listenerNeighboursThread.join();
            threadPinger.join();
            resendThread.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        socket.close();
    }
}
