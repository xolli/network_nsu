package ru.nsu.fit.kazak.network.lab3;

import java.util.HashMap;
import java.util.UUID;

public class NotConfirmedPackets extends HashMap<UUID, InfoPacket> {
    private NotConfirmedPackets() {
        super();
    }

    private static class NotConfirmedPacketHolder {
        private final static NotConfirmedPackets instance = new NotConfirmedPackets();
    }

    public static NotConfirmedPackets getInstance() {
        return NotConfirmedPacketHolder.instance;
    }

    public void addNotConfirmedPacket(Packet packet, InterlocutorNeighbour destination) {
        synchronized (this) {
            put(packet.getUUID(), new InfoPacket(packet, destination));
        }
    }

    public void removeNotConfirmedMessage(UUID uuid) {
        if (containsKey(uuid)) {
            synchronized (this) {
                remove(uuid);
            }
        }
    }
}
