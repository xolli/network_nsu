package ru.nsu.fit.kazak.network.lab3;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ListenerNewMessage implements Runnable {
    private final static int LENGTH_BUFFER = 1024;
    private final InterlocutorPeer peer;
    private final DatagramSocket serverSocket;
    private final NotConfirmedPackets notConfirmedPackets;

    public ListenerNewMessage(InterlocutorPeer peer, DatagramSocket socket, NotConfirmedPackets notConfirmedPackets) {
        this.peer = peer;
        serverSocket = socket;
        this.notConfirmedPackets = notConfirmedPackets;
    }

    public void run() {
        ControllerWork controller = ControllerWork.getInstance();
        try {
            byte[] receivingDataBuffer = new byte[LENGTH_BUFFER];
            DatagramPacket inputPacket = new DatagramPacket(receivingDataBuffer, receivingDataBuffer.length);
            while (controller.canContinueWork()) {
                serverSocket.receive(inputPacket);
                Packet newPacket = byteToPacket(inputPacket.getData());
                assert newPacket != null;
                peer.acceptPacket(newPacket, new InterlocutorNeighbour(inputPacket.getAddress().getHostAddress(), inputPacket.getPort()));
                if (newPacket.isConfirmationPacket()) {
                    System.out.println("get confirm message with id " + newPacket.getUUID());
                    notConfirmedPackets.removeNotConfirmedMessage(newPacket.getUUID());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // https://stackoverflow.com/questions/2836646/java-serializable-object-to-byte-array#2836659
    private Packet byteToPacket(byte[] bytes) {
        ObjectInput in = null;
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            in = new ObjectInputStream(bis);
            return (Packet) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                // ignore close exception
            }
        }
        return null;
    }
}
