package ru.nsu.fit.kazak.network.lab3;

import java.io.Serializable;

public class InterlocutorNeighbour implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String ip;
    private final int port;

    public InterlocutorNeighbour(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        InterlocutorNeighbour otherInterlocutorNeighbour = (InterlocutorNeighbour) obj;
        return this.hashCode() == otherInterlocutorNeighbour.hashCode() ||
                (this.port == otherInterlocutorNeighbour.port && this.ip.equals(otherInterlocutorNeighbour.ip));
    }

    @Override
    public int hashCode() {
        return ip.hashCode() + port;
    }
}
