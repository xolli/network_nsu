package ru.nsu.fit.kazak.network.lab3;

import java.util.UUID;

public class Resender implements Runnable {
    private static final int RESEND_PERIOD = 2;
    private final NotConfirmedPackets notConfirmedPackets;
    private final Sender sender;

    public Resender(NotConfirmedPackets notConfirmedPackets, Sender sender) {
        this.notConfirmedPackets = notConfirmedPackets;
        this.sender = sender;
    }

    @Override
    public void run() {
        ControllerWork controller = ControllerWork.getInstance();
        Neighbours neighbours = Neighbours.getInstance();
        while(controller.canContinueWork()) {
            try {
                Thread.sleep(500);
                for (UUID id : notConfirmedPackets.keySet()) {
                    InfoPacket info = notConfirmedPackets.get(id);
                    if (neighbours.isDied(info.getDestination())) {
                        notConfirmedPackets.removeNotConfirmedMessage(info.getPacket().getUUID());
                        break;
                    }
                    if (info.getTimer() > RESEND_PERIOD) {
                        System.out.println("RESEND to " + info.getDestination().getPort() + " packet type " + info.getPacket().getType() + " id : " + info.getPacket().getUUID());
                        sender.sendPacket(info.getDestination(), info.getPacket());
                        info.resend();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
