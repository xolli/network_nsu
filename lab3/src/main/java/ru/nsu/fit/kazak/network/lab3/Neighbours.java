package ru.nsu.fit.kazak.network.lab3;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Neighbours {
    private final HashSet<InterlocutorNeighbour> lifeNeighbours;
    private final HashMap<InterlocutorNeighbour, InterlocutorNeighbour> neighbourDeputies;
    private final HashSet<InterlocutorNeighbour> diedNeighbours;
    private InterlocutorNeighbour deputy;
    private final Object syncObj = new Object();

    private Neighbours() {
        lifeNeighbours = new HashSet<>();
        diedNeighbours = new HashSet<>();
        neighbourDeputies = new HashMap<>();
    }

    private static class NeighboursHolder {
        private final static Neighbours instance = new Neighbours();
    }

    public static Neighbours getInstance() {
        return NeighboursHolder.instance;
    }

    public void setDeputy(InterlocutorNeighbour newDeputy) {
        synchronized (syncObj) {
            this.deputy = newDeputy;
        }
        System.out.println("My deputy is " + newDeputy.getPort());
    }

    public void removeDeputy() {
        synchronized (syncObj) {
            deputy = null;
        }
        System.out.println("I have not deputy");

    }

    public boolean haveDeputy() {
        return deputy != null;
    }

    public void addNeighbour(InterlocutorNeighbour newNeighbour) {
        synchronized (lifeNeighbours) {
            lifeNeighbours.add(newNeighbour);
        }
    }

    public void markAsKilled(InterlocutorNeighbour removedNeighbour) {
        System.out.println("remove " + removedNeighbour.getPort() + " from lived");
        synchronized (lifeNeighbours) {
            lifeNeighbours.remove(removedNeighbour);
        }
        System.out.println("add " + removedNeighbour.getPort() + " from died");
        synchronized (diedNeighbours) {
            diedNeighbours.add(removedNeighbour);
        }
    }

    public boolean isDied(InterlocutorNeighbour neighbour) {
        return diedNeighbours.contains(neighbour);
    }

    public void revive(InterlocutorNeighbour neighbour) {
        lifeNeighbours.add(neighbour);
        diedNeighbours.remove(neighbour);
    }

    public Set<InterlocutorNeighbour> getLifeNeighboursSet() {
        return lifeNeighbours;
    }

    public InterlocutorNeighbour getDeputy() {
        return deputy;
    }

    public boolean isDeputy(InterlocutorNeighbour neighbour) {
        return neighbour.equals(deputy);
    }

    public void setRandomDeputy() {
        Iterator<InterlocutorNeighbour> it = lifeNeighbours.iterator();
        if (it.hasNext()) {
            setDeputy(it.next());
        } else {
            removeDeputy();
        }
    }

    public boolean haveReplace(InterlocutorNeighbour neighbour) {
        return neighbourDeputies.containsKey(neighbour);
    }

    public InterlocutorNeighbour getReplace(InterlocutorNeighbour oldNeighbour) {
        assert neighbourDeputies.get(oldNeighbour) != null;
        return neighbourDeputies.get(oldNeighbour);
    }

    public void addReplace(InterlocutorNeighbour myNeighbour, InterlocutorNeighbour neighbourReplace) {
        neighbourDeputies.put(myNeighbour, neighbourReplace);
    }

    public void delNeighbourReplace(InterlocutorNeighbour neighbour) {
        neighbourDeputies.remove(neighbour);
    }
}
