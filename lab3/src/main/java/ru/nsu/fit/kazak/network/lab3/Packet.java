package ru.nsu.fit.kazak.network.lab3;

import java.io.Serializable;
import java.util.UUID;

enum TypePacket {TEXT, CONFIRMATION, NEW_NEIGHBOUR, I_AM_ALIVE, MY_DEPUTY, NO_DEPUTY, END_OF_WORK}

public class Packet implements Serializable {
    private static final long serialVersionUID = 2L;
    private final TypePacket type;
    private String text;
    private String name;
    private final UUID uuid;
    private InterlocutorNeighbour deputy;

    public Packet(String name, String text) {
        type = TypePacket.TEXT;
        this.name = name;
        this.text = text;
        this.uuid = UUID.randomUUID();
    }

    public Packet() {
        this.type = TypePacket.NEW_NEIGHBOUR;
        this.uuid = UUID.randomUUID();
    }

    public Packet(Packet otherPacket) {
        this.type = otherPacket.type;
        this.text = otherPacket.text;
        this.name = otherPacket.name;
        this.deputy = otherPacket.deputy;
        this.uuid = UUID.randomUUID();
    }

    public Packet (InterlocutorNeighbour deputy) {
        this(TypePacket.MY_DEPUTY);
        this.deputy = deputy;
    }

    public Packet (TypePacket type) {
        this.type = type;
        this.uuid = UUID.randomUUID();
    }

    public Packet(UUID uuid) {
        this.type = TypePacket.CONFIRMATION;
        this.uuid = uuid;
    }

    public UUID getUUID() {
        return uuid;
    }

    public String getText() {
        return text;
    }

    public String getName() {
        return name;
    }

    public boolean isTextPacket() {
        return type == TypePacket.TEXT;
    }

    public boolean isConfirmationPacket() {
        return type == TypePacket.CONFIRMATION;
    }

    public boolean isNewNeighbourPacket() {
        return type == TypePacket.NEW_NEIGHBOUR;
    }

    public boolean isLifePacket() {
        return type == TypePacket.I_AM_ALIVE;
    }

    public boolean isDeputyPacket() {
        return type == TypePacket.MY_DEPUTY;
    }

    public boolean isNoDeputyPacket() {
        return type == TypePacket.NO_DEPUTY;
    }

    public InterlocutorNeighbour getDeputy() {
        return deputy;
    }

    public TypePacket getType() {
        return type;
    }
}
