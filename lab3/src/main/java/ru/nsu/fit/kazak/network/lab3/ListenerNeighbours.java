package ru.nsu.fit.kazak.network.lab3;

import java.time.Instant;
import java.util.HashMap;

public class ListenerNeighbours implements Runnable {
    static private final long MAX_NO_SAY_PERIOD = 10;
    private final HashMap<InterlocutorNeighbour, Long> lastActivityNeighbours;
    private final Neighbours neighbours;
    private final InterlocutorPeer peer;
    private final Sender sender;

    public ListenerNeighbours(InterlocutorPeer peer, Sender sender) {
        lastActivityNeighbours = new HashMap<>();
        neighbours = Neighbours.getInstance();
        this.peer = peer;
        this.sender = sender;
    }

    @Override
    public void run() {
        ControllerWork controller = ControllerWork.getInstance();
        try {
            while (controller.canContinueWork()) {
                Thread.sleep(1000);
                for (InterlocutorNeighbour neighbour : lastActivityNeighbours.keySet()) {
                    if (nowTime() - lastActivityNeighbours.get(neighbour) > MAX_NO_SAY_PERIOD) {
                        System.out.println("Neighbor " + neighbour.getPort() + " is die =(");
                        acceptDie(neighbour);
                        break;
                    }
                }
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private void acceptDie(InterlocutorNeighbour killedNeighbour) {
        neighbours.markAsKilled(killedNeighbour);
        if (neighbours.isDeputy(killedNeighbour)) {
            neighbours.setRandomDeputy();
            peer.notifyAllAboutDeputy();
        }
        lastActivityNeighbours.remove(killedNeighbour);
        replaceNeighbour(killedNeighbour);
    }

    private void replaceNeighbour(InterlocutorNeighbour replacedNeighbour) {
        if (neighbours.haveReplace(replacedNeighbour)) {
            neighbours.addNeighbour(neighbours.getReplace(replacedNeighbour));
            sender.sendNeighbourRequest(neighbours.getReplace(replacedNeighbour));
            neighbours.delNeighbourReplace(replacedNeighbour);
        }
    }

    public void updateActivity(InterlocutorNeighbour neighbour) {
        lastActivityNeighbours.put(neighbour, nowTime());
    }

    public long nowTime() {
        return Instant.now().getEpochSecond();
    }
}
