package ru.nsu.fit.kazak.network.lab3;

public class Main {
    public static void main(String[] args) {
        if (args.length != 3 && args.length != 5) {
            System.err.println("Arguments error =(\nexpected: lab3 <name> <port> <packet loss percentage> [<neighbor's address> <neighbor's port>]");
            return;
        }
        String name = args[0];
        int port;
        int percentLoss;
        try {
            port = Integer.parseInt(args[1]);
            percentLoss = Integer.parseInt(args[2]);
        } catch (NumberFormatException ex) {
            System.err.println("False arguments =(");
            return;
        }
        if (port < 1 || port > 65535 || percentLoss < 0 || percentLoss > 100) {
            System.err.println("False arguments =(");
            return;
        }
        InterlocutorPeer peer;
        try {
            if (args.length == 3) {
                peer = new InterlocutorPeer(name, port, percentLoss);
            } else {
                int portNeighbour = Integer.parseInt(args[4]);
                peer = new InterlocutorPeer(name, port, new InterlocutorNeighbour(args[3], portNeighbour), percentLoss);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
        peer.startListen();
        peer.speak();
        peer.close();
    }

}
