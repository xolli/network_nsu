package ru.nsu.fit.kazak.network.lab3;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class Sender {
    private final DatagramSocket clientSocket;
    private final int percentLoss;
    private final NotConfirmedPackets notConfirmedPackets;

    public Sender(DatagramSocket socket, int percentLoss) {
        clientSocket = socket;
        this.percentLoss = percentLoss;
        notConfirmedPackets = NotConfirmedPackets.getInstance();
    }

    public void sendMessage(InterlocutorNeighbour destination, String peerName, String message) {
        Packet packet = new Packet(peerName, message);
        sendPacket(destination, packet);
    }

    public void sendConfirmed (InterlocutorNeighbour destination, UUID idMessage){
        Packet packet = new Packet(idMessage);
        sendPacketWithoutLoss(destination, packet);
        System.out.println("Send confirm with id " + idMessage);
    }

    public void sendNeighbourRequest(InterlocutorNeighbour newNeighbour){
        Packet packet = new Packet();
        System.out.println("Send packet with id: " + packet.getUUID());
        sendPacket(newNeighbour, packet);
    }

    public void sendPacket(InterlocutorNeighbour destination, Packet packet) {
        notConfirmedPackets.addNotConfirmedPacket(packet, destination);
        if (simulateLoss()) {
            return;
        }
        sendPacketWithoutLoss(destination, packet);
    }

    public void sendPacketWithoutConfirm (InterlocutorNeighbour destination, Packet packet) {
        if (simulateLoss()) {
            return;
        }
        sendPacketWithoutLoss(destination, packet);
    }

    public void sendPacketWithoutLoss(InterlocutorNeighbour destination, Packet packet) {
        try {
            InetAddress senderAddress = InetAddress.getByName(destination.getIp());
            byte[] sendingDataBuffer = packetToByte(packet);
            assert sendingDataBuffer != null;
            DatagramPacket newPacket = new DatagramPacket(sendingDataBuffer, sendingDataBuffer.length, senderAddress, destination.getPort());
            clientSocket.send(newPacket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // https://stackoverflow.com/questions/2836646/java-serializable-object-to-byte-array#2836659
    private byte[] packetToByte(Packet packet) {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            ObjectOutputStream out = new ObjectOutputStream(bos);
            out.writeObject(packet);
            out.flush();
            return bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        // ignore close exception
    }

    private boolean simulateLoss() {
        return generateRandomPercent() <= percentLoss;
    }

    private int generateRandomPercent() {
        return ThreadLocalRandom.current().nextInt(1, 101);
    }

    public void sendMyDeputy (InterlocutorNeighbour destination, InterlocutorNeighbour deputy) {
        sendPacket(destination, new Packet(deputy));
    }
}