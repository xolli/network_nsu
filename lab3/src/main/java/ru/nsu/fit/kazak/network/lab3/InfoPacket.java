package ru.nsu.fit.kazak.network.lab3;

import java.time.Instant;

public class InfoPacket {
    private final InterlocutorNeighbour destination;
    private final Packet packet;
    private long time_send;

    public InfoPacket(Packet packet, InterlocutorNeighbour destination) {
        this.packet = packet;
        this.destination = destination;
        time_send = Instant.now().getEpochSecond();
    }

    public Packet getPacket() {
        return packet;
    }

    public InterlocutorNeighbour getDestination() {
        return destination;
    }

    public void resend() {
        time_send = Instant.now().getEpochSecond();
    }

    public long getTimer() {
        return Instant.now().getEpochSecond() - time_send;
    }
}
