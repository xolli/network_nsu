package ru.nsu.fit.kazak.network.lab2.client;

public class Main {
    public static void main(String[] args) {
        if (args.length < 3) {
            System.err.println("Wrong arguments\nexpected: filname address port");
            return;
        }
        String filename = args[0];
        String address = args[1];
        int port;
        try {
            port = Integer.parseInt(args[2]);
        } catch (NumberFormatException ex) {
            System.err.println("Incorrect port =(");
            return;
        }
        Client client = new Client(filename, address, port);
        Thread main = new Thread(client);
        main.start();
    }
}
