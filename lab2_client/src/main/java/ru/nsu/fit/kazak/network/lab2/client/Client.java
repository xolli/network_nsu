package ru.nsu.fit.kazak.network.lab2.client;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;

public class Client implements Runnable {
    private final String address;
    private final File file;
    private final int port;

    public Client(String filePath, String address, int port) {
        this.address = address;
        this.port = port;
        this.file = new File(filePath);
    }

    private void sendFileInfo(OutputStream out) throws IOException {
        byte[] sizeBytes = convertLongToByteArray(file.length());
        out.write(sizeBytes, 0, 8);
        out.write(file.getName().length());
        out.write(file.getName().getBytes());
    }

    private void sendFile(OutputStream out) throws IOException {
        FileInputStream reader = new FileInputStream(file);
        byte[] buff = new byte[1024];
        int countReadBytes;
        while ((countReadBytes = reader.read(buff, 0, 1024)) > 0) {
            out.write(buff, 0, countReadBytes);
        }
    }

    private void checkSending(Socket socket) throws IOException {
        InputStream in = socket.getInputStream();
        byte[] answerByte = new byte[2];
        if (in.read(answerByte, 0, 2) != 2) {
            System.out.println("***Not Ok***");
            return;
        }
        String answer = new String(answerByte, 0, 2);
        if (answer.equals("ok")) {
            System.out.println("***OK***");
        } else {
            System.out.println("***Not Ok***");
        }
    }

    @Override
    public void run() {
        try {
            Socket socket = new Socket(address, port);
            OutputStream out = socket.getOutputStream();
            System.out.println("Connected successfully to " + address + ":" + port);
            sendFileInfo(out);
            sendFile(out);
            checkSending(socket);
        } catch (IOException e) {
            System.err.println(e.getMessage() + " =(");
        }
    }

    public static byte[] convertLongToByteArray(long value) {
        return  ByteBuffer.allocate(8).putLong(value).array();
    }
}
