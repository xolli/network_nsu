package ru.nsu.fit.kazak.network.lab2.server;

public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Wrong argument =(\nexpected: port");
            return;
        }
        int port;
        try {
            port = Integer.parseInt(args[0]);
        } catch (NumberFormatException ex) {
            System.err.println("Incorrect port =(");
            return;
        }
        FileServer server = new FileServer(port);
//        Thread main = new Thread(server);
        server.start();
        StopWindow stopWindow = new StopWindow(server);
        stopWindow.showWindow();
    }
}
