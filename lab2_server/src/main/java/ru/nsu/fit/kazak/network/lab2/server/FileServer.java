package ru.nsu.fit.kazak.network.lab2.server;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class FileServer extends Thread {
    private final AtomicBoolean stopped;
    private final int serverPort;
    private final Set<FileServerHandler> workers;

    public FileServer (int serverPort) {
        this.serverPort = serverPort;
        createDir();
        stopped = new AtomicBoolean(false);
        workers = new HashSet<>();
    }

    private void createDir() {
        File dir = new File ("uploads");
        if (!dir.exists()) {
            try {
                dir.mkdir();
            } catch (SecurityException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void stopServer() {
        try {
            stopped.set(true);
            Socket socket = new Socket("127.0.0.1", serverPort);
            socket.close();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    private void stopAllWorkers() throws IOException {
        synchronized (workers) {
            for (FileServerHandler worker : workers) {
                worker.stopReceive();
            }
        }
    }

    @Override

    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(serverPort, 10);
            Socket socketHandler;
            System.out.println("Start");
            while(true) {
                socketHandler = serverSocket.accept();
                if (stopped.get()) {
                    System.out.println("Stopped server");
                    serverSocket.close();
                    stopAllWorkers();
                    return;
                }
                System.out.println("new client");
                FileServerHandler worker = new FileServerHandler(socketHandler);
                worker.start();
                synchronized (workers) {
                    workers.add(worker);
                }
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
