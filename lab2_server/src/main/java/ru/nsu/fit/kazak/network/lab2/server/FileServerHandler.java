package ru.nsu.fit.kazak.network.lab2.server;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicBoolean;

public class FileServerHandler extends Thread {
    private final Socket socket;
    private final InputStream in;
    private final AtomicBoolean stopped;
    private OutputStream out;

    public FileServerHandler(Socket socket) throws IOException {
        this.socket = socket;
        in = socket.getInputStream();
        stopped = new AtomicBoolean(false);
        out = socket.getOutputStream();
    }

    private long getFileSize() throws IOException {
        byte[] sizeBytes = new byte[8];
        in.read(sizeBytes, 0, 8);
        return convertByteToLong(sizeBytes);
    }

    private String getFilename() throws IOException {
        int lenFileName = in.read(); // len filename in BYTES
        byte[] filenameByte = new byte[lenFileName];
        in.read(filenameByte, 0, lenFileName);
        return new String(filenameByte, 0, lenFileName);
    }

    private File createFile(String filename) throws IOException {
        File file = new File("uploads/" + filename);
        System.out.println("start receive file:" + file.getName());
        file.createNewFile();
        return file;
    }

    private long receiveFile(File file, long fileSize) throws IOException {
        FileOutputStream out = new FileOutputStream(file);
        int countBytes;
        byte[] buff = new byte[1024];
        long delta_t = System.nanoTime();
        boolean first = true;
        long recivedBytes = 0;
        double beginTime = System.nanoTime();
        long bytes3sec = 0;
        while (recivedBytes < fileSize && (countBytes = in.read(buff, 0, 1024)) > 0) {
            recivedBytes += countBytes;
            bytes3sec += countBytes;
            out.write(buff, 0, countBytes);
            double d = (System.nanoTime() - delta_t) / 1E9;
            if (d >= 3 || first) {
                first = false;
                System.out.println(" >> Speed now: " + ((double)bytes3sec) / d + " byte/sec");
                delta_t = System.nanoTime(); // Set to zero
                double allTime = (delta_t - beginTime) / 1E9;
                System.out.println(" >> Speed med: " + ((double)recivedBytes) / allTime + " byte/sec\n");
                bytes3sec = 0;
            }
        }
        return recivedBytes;
    }

    private void checkSending(long receivedBytes, long fileSize) throws IOException {
        if (receivedBytes == fileSize) {
            sayOk();
            System.out.println("Ok receive file");
        } else {
            sayNotOk();
            System.err.println("Error receive file");
        }
    }

    private void closeConnect() {
        try {
            in.close();
            socket.close();
            stopped.set(true);
            out.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public void run() {
        try {
            long fileSize = getFileSize();
            String filename = getFilename();
            File file = createFile(filename);
            long receivedBytes = receiveFile(file, fileSize);
            checkSending(receivedBytes, fileSize);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            closeConnect();
        }
    }

    private void sayNotOk() throws IOException {
        out.write("ko".getBytes());
    }

    private void sayOk() throws IOException {
        out.write("ok".getBytes());
    }

    private long convertByteToLong(byte[] array) {
        return ByteBuffer.wrap(array).getLong();
    }

    public void stopReceive() throws IOException {
        if (!stopped.get()) {
            closeConnect();
        }
    }
}
