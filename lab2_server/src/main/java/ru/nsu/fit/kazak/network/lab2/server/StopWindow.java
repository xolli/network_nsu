package ru.nsu.fit.kazak.network.lab2.server;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class StopWindow extends JFrame {
    private JPanel controlPanel;

    public StopWindow(FileServer serverThread) {
        super("stop window");
        setSize(200, 200);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent){
                System.exit(0);
            }
        });

        JButton stopButton = new JButton("stop server");
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                serverThread.stopServer();
                setVisible(false);
                dispose();
            }
        });
        controlPanel = new JPanel();
        controlPanel.add(stopButton);
        add(controlPanel);
    }

    public void showWindow() {
        setVisible(true);
    }
}
