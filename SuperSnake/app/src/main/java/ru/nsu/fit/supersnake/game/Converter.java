package ru.nsu.fit.supersnake.game;

import com.google.protobuf.InvalidProtocolBufferException;

import java.net.DatagramPacket;
import java.util.Arrays;
import java.util.HashSet;

import me.ippolitov.fit.snakes.SnakesProto;
import ru.nsu.fit.supersnake.Exceptions.IncorrectAnnoncedMessage;
import ru.nsu.fit.supersnake.Exceptions.IncorrectMasterPort;
import ru.nsu.fit.supersnake.game.message.AnnouncementMsg;
import ru.nsu.fit.supersnake.game.message.Message;
import ru.nsu.fit.supersnake.game.network.GeneratorMessageId;

public class Converter {
    public static Message createStateMsg(Field field, String ip, int port) {
        SnakesProto.GameState.Builder gameState = SnakesProto.GameState.newBuilder();
        field.startRead();
        gameState.setStateOrder(field.getStateOrder());
        for (Snake snake : field.getSnakes()) {
            SnakesProto.GameState.Snake.Builder snakeBuilder = SnakesProto.GameState.Snake.newBuilder().
                    setState(snake.getState()).setPlayerId(snake.getPlayerId()).setHeadDirection(snake.getHeadDirection());
            for (Coord point : snake.getPoints()) {
                snakeBuilder.addPoints(SnakesProto.GameState.Coord.newBuilder().setX(point.getX()).setY(point.getY()));
            }
            gameState.addSnakes(snakeBuilder);
        }
        for (Coord food : field.getFoods()) {
            gameState.addFoods(SnakesProto.GameState.Coord.newBuilder().setX(food.getX()).setY(food.getY()));
        }
        SnakesProto.GamePlayers.Builder gamePlayers = SnakesProto.GamePlayers.newBuilder();
        for (Player player : field.getPlayers()) {
            SnakesProto.NodeRole role;
            if (player.getNodeRole() == NodeRole.NORMAL) {
                role = SnakesProto.NodeRole.NORMAL;
            } else if (player.getNodeRole() == NodeRole.MASTER) {
                role = SnakesProto.NodeRole.MASTER;
            } else if (player.getNodeRole() == NodeRole.DEPUTY) {
                role = SnakesProto.NodeRole.DEPUTY;
            } else {
                role = SnakesProto.NodeRole.VIEWER;
            }
            gamePlayers.addPlayers(SnakesProto.GamePlayer.newBuilder().setName(player.getName()).
                    setId(player.getId()).setIpAddress(player.getIp()).setPort(player.getPort()).
                    setRole(role).setType(SnakesProto.PlayerType.HUMAN).setScore(player.getScore()));
        }
        try {
            gameState.setPlayers(gamePlayers).setConfig(convertConfig(field.getConfig()));
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        field.stopRead();
        SnakesProto.GameMessage.StateMsg.Builder stateMsg = SnakesProto.GameMessage.StateMsg.newBuilder();
        stateMsg.setState(gameState);
        long msgSeq = GeneratorMessageId.getInstance().getNewId();
        return new Message(ip, port, SnakesProto.GameMessage.newBuilder().setState(stateMsg).setMsgSeq(msgSeq).build().toByteArray(), msgSeq, SnakesProto.GameMessage.TypeCase.STATE);
    }

    private static SnakesProto.GameConfig convertConfig(ConfigServer config) {
        return SnakesProto.GameConfig.newBuilder().
                setWidth(config.getWidth()).setHeight(config.getHeight()).
                setFoodStatic(config.getFoodStatic()).setFoodPerPlayer(config.getFoodPerPlayer()).
                setStateDelayMs(config.getStateDelayMs()).setDeadFoodProb(config.getDeadFoodProb()).
                setPingDelayMs(config.getPingDelayMs()).setNodeTimeoutMs(config.getNodeTimeoutMs()).build();
    }

    public static NodeRole convertNodeRole(SnakesProto.NodeRole role) {
        switch (role) {
            case NORMAL: return NodeRole.NORMAL;
            case DEPUTY: return NodeRole.DEPUTY;
            case MASTER: return NodeRole.MASTER;
            case VIEWER: return NodeRole.VIEWER;
        }
        return NodeRole.NORMAL; // надеюсь что до сюда код никогда не дойдёт
    }

    public static SnakesProto.NodeRole convertNodeRole (NodeRole role) {
        switch (role) {
            case NORMAL: return SnakesProto.NodeRole.NORMAL;
            case MASTER: return SnakesProto.NodeRole.MASTER;
            case VIEWER: return SnakesProto.NodeRole.VIEWER;
            case DEPUTY: return SnakesProto.NodeRole.DEPUTY;
        }
        return null;
    }

    public static ConfigServer convertConfig(SnakesProto.GameConfig protoConfig) {
        return new ConfigServer(protoConfig.getWidth(), protoConfig.getHeight(), protoConfig.getFoodStatic(),
                protoConfig.getFoodPerPlayer(), protoConfig.getStateDelayMs(), protoConfig.getDeadFoodProb(),
                protoConfig.getPingDelayMs(), protoConfig.getNodeTimeoutMs());
    }

    public static Message createRoleChangeMessage(NodeRole senderRole, NodeRole receiverRole,
                                                  String destinationIp, int destinationPort,
                                                  int senderId, int receiverId
                                                  ) {
        long msgSeq = GeneratorMessageId.getInstance().getNewId();
        SnakesProto.NodeRole senderRoleProto = convertNodeRole(senderRole);
        SnakesProto.NodeRole receiverRoleProto = convertNodeRole(receiverRole);
        SnakesProto.GameMessage.RoleChangeMsg roleChangeMsgOrBuilder = SnakesProto.GameMessage.
                RoleChangeMsg.newBuilder().setSenderRole(senderRoleProto).setReceiverRole(receiverRoleProto).build();
        return new Message(destinationIp, destinationPort, SnakesProto.GameMessage.newBuilder().
                setMsgSeq(msgSeq).setRoleChange(roleChangeMsgOrBuilder).setSenderId(senderId).
                setReceiverId(receiverId).build().toByteArray(), msgSeq, SnakesProto.GameMessage.TypeCase.ROLE_CHANGE);
    }

    public enum PlayerType {
        HUMAN,
        ROBOT
    }

    public static byte[] getAnnouncementMsg(Field field, ConfigServer config) {
        SnakesProto.GamePlayers.Builder playersProto = SnakesProto.GamePlayers.newBuilder();
        field.startRead();
        for (Player player : field.getPlayers()) {
            SnakesProto.NodeRole role;
            if (player.getNodeRole() == NodeRole.NORMAL) {
                role = SnakesProto.NodeRole.NORMAL;
            } else if (player.getNodeRole() == NodeRole.MASTER) {
                role = SnakesProto.NodeRole.MASTER;
            } else if (player.getNodeRole() == NodeRole.DEPUTY) {
                role = SnakesProto.NodeRole.DEPUTY;
            } else {
                role = SnakesProto.NodeRole.VIEWER;
            }
            playersProto.addPlayers(SnakesProto.GamePlayer.newBuilder().setName(player.getName()).
                    setId(player.getId()).setIpAddress(player.getIp()).setPort(player.getPort()).
                    setRole(role).setType(SnakesProto.PlayerType.HUMAN).setScore(player.getScore()).build());
        }
        field.stopRead();
        SnakesProto.GameConfig configProto = SnakesProto.GameConfig.newBuilder().
                setWidth(config.getWidth()).setHeight(config.getHeight()).
                setFoodStatic(config.getFoodStatic()).setFoodPerPlayer(config.getFoodPerPlayer()).
                setStateDelayMs(config.getStateDelayMs()).setDeadFoodProb(config.getDeadFoodProb()).
                setPingDelayMs(config.getPingDelayMs()).setNodeTimeoutMs(config.getNodeTimeoutMs()).build();
        return SnakesProto.GameMessage.AnnouncementMsg.newBuilder().setPlayers(playersProto).setConfig(configProto).build().toByteArray();
    }

    public static AnnouncementMsg convertAnnouncementMsg(DatagramPacket announcementMsgPacket) throws InvalidProtocolBufferException, IncorrectAnnoncedMessage, IncorrectMasterPort {
        SnakesProto.GameMessage.AnnouncementMsg msg = SnakesProto.GameMessage.AnnouncementMsg.parseFrom(Arrays.copyOfRange(announcementMsgPacket.getData(), 0, announcementMsgPacket.getLength()));
        HashSet<Player> players = new HashSet<>(msg.getPlayers().getPlayersCount());
        for (SnakesProto.GamePlayer player : msg.getPlayers().getPlayersList()) {
            NodeRole role;
            if (player.getRole() == SnakesProto.NodeRole.NORMAL) {
                role = NodeRole.NORMAL;
            } else if (player.getRole() == SnakesProto.NodeRole.MASTER) {
                role = NodeRole.MASTER;
            } else if (player.getRole() ==SnakesProto. NodeRole.DEPUTY) {
                role = NodeRole.DEPUTY;
            } else {
                role = NodeRole.VIEWER;
            }
            players.add(new Player(player.getName(), player.getId(), player.getIpAddress(), player.getPort(), role));
        }
        ConfigServer config = new ConfigServer(msg.getConfig().getWidth(), msg.getConfig().getHeight(),
                msg.getConfig().getFoodStatic(), msg.getConfig().getFoodPerPlayer(),
                msg.getConfig().getStateDelayMs(), msg.getConfig().getDeadFoodProb(),
                msg.getConfig().getPingDelayMs(), msg.getConfig().getNodeTimeoutMs());
        return new AnnouncementMsg(players, config, announcementMsgPacket.getAddress().getHostAddress(), getMasterPort(players));
    }

    private static int getMasterPort(HashSet<Player> players) throws IncorrectAnnoncedMessage, IncorrectMasterPort {
        for (Player player : players) {
            if (player.getNodeRole() == NodeRole.MASTER) {
                if (player.getPort() < 0) {
                    throw new IncorrectMasterPort();
                }
                return player.getPort();
            }
        }
        throw new IncorrectAnnoncedMessage("Incorrect AnnouncementMsg (MASTER is absent)");
    }

    public static SnakesProto.GameMessage convertMessage(DatagramPacket packet) throws InvalidProtocolBufferException {
        return SnakesProto.GameMessage.parseFrom(Arrays.copyOfRange(packet.getData(), 0, packet.getLength()));
    }

    public static Message createJoinMessage(PlayerType type, boolean onlyView, String username, String destinationIp, int destinationPort) {
        SnakesProto.GameMessage.JoinMsg.Builder joinMsgBuilder = SnakesProto.GameMessage.JoinMsg.newBuilder().setName(username);
        switch (type) {
            case HUMAN: joinMsgBuilder.setPlayerType(SnakesProto.PlayerType.HUMAN);
            break;
            case ROBOT: joinMsgBuilder.setPlayerType(SnakesProto.PlayerType.ROBOT);
        }
        joinMsgBuilder.setOnlyView(onlyView);
        long msgSeq = GeneratorMessageId.getInstance().getNewId();
        return new Message(destinationIp, destinationPort, SnakesProto.GameMessage.newBuilder().setJoin(joinMsgBuilder).setMsgSeq(msgSeq).build().toByteArray(), msgSeq, SnakesProto.GameMessage.TypeCase.JOIN);
    }

    public static Message createSnakeStateMessage(SnakesProto.Direction direction, String destinationIp, int destinationPort) {
        SnakesProto.GameMessage.SteerMsg.Builder steer = SnakesProto.GameMessage.SteerMsg.newBuilder().setDirection(direction);
        long msgSeq = GeneratorMessageId.getInstance().getNewId();
        return new Message(destinationIp, destinationPort, SnakesProto.GameMessage.newBuilder().setSteer(steer).setMsgSeq(msgSeq).build().toByteArray(), msgSeq, SnakesProto.GameMessage.TypeCase.STEER);
    }

    public static Message createPingMsg(String destinationIp, int destinationPort) {
        long msgSeq = GeneratorMessageId.getInstance().getNewId();
        return new Message(destinationIp, destinationPort, SnakesProto.GameMessage.newBuilder().setPing(SnakesProto.GameMessage.PingMsg.newBuilder()).setMsgSeq(msgSeq).build().toByteArray(), msgSeq, SnakesProto.GameMessage.TypeCase.PING);
    }

    public static Message createAckMsgNewMsgSeq(String destinationIp, int destinationPort, int senderId, int receiverId) {
        long msgSeq = GeneratorMessageId.getInstance().getNewId();
        SnakesProto.GameMessage.AckMsg ackMsg = SnakesProto.GameMessage.AckMsg.newBuilder().build();
        return new Message(destinationIp, destinationPort, SnakesProto.GameMessage.newBuilder().setAck(ackMsg).setMsgSeq(msgSeq).setSenderId(senderId).setReceiverId(receiverId).build().toByteArray(), msgSeq, SnakesProto.GameMessage.TypeCase.ACK);
    }

    public static Message createAckMsg(String destinationIp, int destinationPort, long msgSeq, int senderId, int receiverId) {
        SnakesProto.GameMessage.AckMsg ackMsg = SnakesProto.GameMessage.AckMsg.newBuilder().build();
        return new Message(destinationIp, destinationPort, SnakesProto.GameMessage.newBuilder().setAck(ackMsg).setMsgSeq(msgSeq).setSenderId(senderId).setReceiverId(receiverId).build().toByteArray(), msgSeq, SnakesProto.GameMessage.TypeCase.ACK);
    }
}
