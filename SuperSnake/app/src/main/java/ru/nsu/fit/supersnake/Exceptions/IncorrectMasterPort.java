package ru.nsu.fit.supersnake.Exceptions;

public class IncorrectMasterPort extends Exception {
    public IncorrectMasterPort() {
        super("Incorrect master's port =(");
    }
}
