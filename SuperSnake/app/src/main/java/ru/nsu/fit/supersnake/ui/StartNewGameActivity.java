package ru.nsu.fit.supersnake.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import java.net.DatagramSocket;
import java.net.SocketException;

import ru.nsu.fit.supersnake.game.ConfigServer;
import ru.nsu.fit.supersnake.game.Controller;
import ru.nsu.fit.supersnake.game.Field;
import ru.nsu.fit.supersnake.game.GameServer;
import ru.nsu.fit.supersnake.game.NodeRole;
import ru.nsu.fit.supersnake.game.Player;
import ru.nsu.fit.supersnake.game.ServerInitState;
import ru.nsu.fit.supersnake.game.network.ListenerLivingNode;
import ru.nsu.fit.supersnake.game.network.Pinger;
import ru.nsu.fit.supersnake.game.network.Resender;
import ru.nsu.fit.supersnake.game.network.Sender;
import ru.nsu.fit.supersnake.game.network.StateSender;
import ru.nsu.fit.supersnake.game.network.UnicastReceiver;


public class StartNewGameActivity extends AppCompatActivity {
    // network
    private final int unicastPort = 50789;
    private DatagramSocket socket;

    // Thread and runnable
    private Thread drawerThread;
    private Thread unicastReceiverThread;
    private Thread stateSenderThread;
    private Thread serverThread;
    private Thread senderThread;
    private Thread resenderThread;
    private Thread pingerThread;
    private Thread listenerLivingNodeThread;
    private Drawer drawer;
    private UnicastReceiver unicastReceiver;
    private GameServer server;
    private Sender sender;
    private Resender resender;
    private Pinger pinger;
    private ListenerLivingNode listenerLivingNode;

    // game specific
    private Field field;
    private Player thisPlayer;
    private Controller controller;

    // graphics
    private GameView gameView;

    // other
    private ConfigServer config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initSocket();
        config = new ConfigServer(this);
        initGame();
        initThreads();
        setContentView(gameView);
    }

    @SuppressLint("ShowToast")
    private void initSocket() {
        try {
            socket = new DatagramSocket(unicastPort);
        } catch (SocketException e) {
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT);
        }
    }

    private void initGame() {
        thisPlayer = createPlayer(config, socket);
        field = new Field(config, thisPlayer);
        server = new GameServer(config, ServerInitState.START, field);
        controller = new Controller(server, thisPlayer);
        gameView = new GameView(this, field, config, controller);
    }

    private void initThreads() {
        sender = new Sender(socket);
        senderThread = new Thread(sender);
        senderThread.start();

        drawer = new Drawer(gameView, field);
        drawerThread = new Thread(drawer);
        drawerThread.start();

        serverThread = new Thread(server);
        serverThread.start();

        unicastReceiver = new UnicastReceiver(socket, field, sender, controller);
        unicastReceiverThread = new Thread(unicastReceiver);
        unicastReceiverThread.start();

        stateSenderThread = new Thread(new StateSender(field, sender));
        stateSenderThread.start();

        resender = new Resender(sender, field);
        resenderThread = new Thread(resender);
        resenderThread.start();

        pinger = new Pinger(field, getPlayerName(), sender);
        pingerThread = new Thread(pinger);
        pingerThread.start();

        listenerLivingNode = new ListenerLivingNode(field, sender, controller);
        listenerLivingNodeThread = new Thread(listenerLivingNode);
        listenerLivingNodeThread.start();
    }

    private Player createPlayer(ConfigServer config, DatagramSocket socket) {
        return new Player(getPlayerName(), 0, "", unicastPort, NodeRole.MASTER);
    }

    private String getPlayerName() {
        SharedPreferences options = getSharedPreferences(SetOptionsActivity.PREFS_NAME, 0);
        return options.getString("username", "user");
    }

    private void stopThreads() {
        if (serverThread != null) {
            serverThread.interrupt();
        }
        if (drawerThread != null) {
            drawerThread.interrupt();
        }
        if (unicastReceiverThread != null) {
            unicastReceiverThread.interrupt();
        }
        if (senderThread != null) {
            senderThread.interrupt();
        }
        if (socket != null) {
            socket.close();
        }
        if (stateSenderThread != null) {
            stateSenderThread.interrupt();
        }
        if (resenderThread != null) {
            resenderThread.interrupt();
        }
        if (pingerThread != null) {
            pingerThread.interrupt();
        }
        if (listenerLivingNodeThread != null) {
            listenerLivingNodeThread.interrupt();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sender.stop();
        unicastReceiver.stop();
        stopThreads();
    }
}
