package ru.nsu.fit.supersnake.ui;

import android.graphics.Canvas;
import android.graphics.Color;

import java.util.TimerTask;

public class GraphUpdater extends TimerTask {

    private final GameView gameView;

    public GraphUpdater(GameView gameView) {
        this.gameView = gameView;
    }

    @Override
    public void run() {
        Canvas canvas = gameView.getHolder().lockCanvas();
        if (canvas != null){
            canvas.drawColor(Color.BLACK);
            gameView.drawField(canvas);
            gameView.getHolder().unlockCanvasAndPost(canvas);
        }
    }

}
