package ru.nsu.fit.supersnake.game.message;

import me.ippolitov.fit.snakes.SnakesProto;

public class Message {
    private String destinationIp;
    private int destinationPort;
    private byte[] data;
    private long msgSeq;
    public SnakesProto.GameMessage.TypeCase type;

    public Message(String destinationIp, int destinationPort, byte[] data, long msgSeq, SnakesProto.GameMessage.TypeCase type) {
        this.destinationIp = destinationIp;
        this.destinationPort = destinationPort;
        this.data = data;
        this.msgSeq = msgSeq;
        this.type = type;
    }

    public String getDestinationIp() {
        return destinationIp;
    }

    public int getDestinationPort() {
        return destinationPort;
    }

    public byte[] getData() {
        return data;
    }

    public void setDestination(String ip, int port) {
        destinationIp = ip;
        destinationPort = port;
    }

    public long getMsgSeq() {
        return msgSeq;
    }

    @Override
    public String toString() {
        return destinationIp + ":" + destinationPort + ":" + msgSeq + ":" + type;
    }
}
