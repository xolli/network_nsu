package ru.nsu.fit.supersnake.game.network;

import java.util.HashSet;

import ru.nsu.fit.supersnake.game.Controller;
import ru.nsu.fit.supersnake.game.Field;
import ru.nsu.fit.supersnake.game.Converter;
import ru.nsu.fit.supersnake.game.GameServer;
import ru.nsu.fit.supersnake.game.NodeRole;
import ru.nsu.fit.supersnake.game.Player;
import ru.nsu.fit.supersnake.game.ServerInitState;
import ru.nsu.fit.supersnake.game.StoppedRunnable;

public class ListenerLivingNode extends StoppedRunnable {
    private final LivingTime livingTime;
    private final Field field;
    private final Sender sender;
    private final Controller controller;
    private Thread stateSenderThread;

    public ListenerLivingNode(Field field, Sender sender, Controller controller) {
        this.livingTime = LivingTime.getInstance();
        this.field = field;
        this.sender = sender;
        this.controller = controller;
    }

    @Override
    public void run() {
        Thread serverThread = null;
        try {
            int nodeTimeoutMs = field.getConfig().getNodeTimeoutMs();
            NotConfirmedMessages notConfirmedMessages = NotConfirmedMessages.getInstance();
            while (!stopped.get()) {
                Thread.sleep(nodeTimeoutMs);
                synchronized (livingTime) {
                    HashSet<Integer> deletedId = new HashSet<>();
                    for (Integer id : livingTime.keySet()) {
                        if (currentTime() - livingTime.get(id) > nodeTimeoutMs) { // условие отвала вершины
                            field.startRead(); // для синхронзации
                            Player player = field.getPlayersMap().get(id);
                            if (field.getMyRole() == NodeRole.NORMAL && field.getRole(id) == NodeRole.MASTER) {
                                Player newMaster = field.getDeputy();
                                controller.setDestination(newMaster.getIp(), newMaster.getPort());
                                notConfirmedMessages.changeDestination(newMaster.getIp(), newMaster.getPort(),
                                        player.getIp(), player.getPort());
                            } else if (field.getMyRole() == NodeRole.MASTER && field.getRole(id) == NodeRole.DEPUTY) {
                                field.stopRead(); // не знаю, нало ли
                                Player newDeputy = field.updateDeputy();
                                field.infectSnake(id);
                                field.removePlayer(id);
                                field.startRead();
                                if (newDeputy != null) {
                                    sender.sendMessageWithConfirm(Converter.createRoleChangeMessage(NodeRole.MASTER,
                                            NodeRole.DEPUTY, newDeputy.getIp(), newDeputy.getPort(), field.getMyId(), id));
                                }
                            } else if (field.getMyRole() == NodeRole.DEPUTY && field.getRole(id) == NodeRole.MASTER) {
                                field.stopRead();
                                field.becomeMaster();
                                field.infectSnake(id);
                                field.removePlayer(id);
                                GameServer server = new GameServer(field.getConfig(), ServerInitState.CONTINUE, field);
                                controller.setServerMode(server, field.getPlayersMap().get(field.getMyId()));
                                serverThread = new Thread(server);
                                serverThread.start();
                                field.updateDeputy();
                                System.out.println("I am MASTER!");
                                field.startRead();
                                for (Player otherPlayer : field.getPlayers()) {
                                    if (otherPlayer.getNodeRole() != NodeRole.MASTER) {
                                        sender.sendMessageWithConfirm(Converter.createRoleChangeMessage
                                                (NodeRole.MASTER, NodeRole.NORMAL, otherPlayer.getIp(),
                                                        otherPlayer.getPort(), field.getMyId(), otherPlayer.getId()));
                                    }
                                }
                                stateSenderThread = new Thread(new StateSender(field, sender));
                                stateSenderThread.start();
                            } else if (field.getMyRole() == NodeRole.MASTER) {
                                field.infectSnake(id);
                                field.removePlayer(id);
                            }
                            field.stopRead();
                            deletedId.add(id);
                            if (player != null) {
                                notConfirmedMessages.removeReceiver(player.getIp(), player.getPort());
                            }
                        }
                    }
                    for (Integer id : deletedId) {
                        livingTime.remove(id);
                    }
                    deletedId.clear();
                }
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } finally {
            if (serverThread != null) {
                serverThread.interrupt();
            }
            if (stateSenderThread != null) {
                stateSenderThread.interrupt();
            }
        }
    }

    private long currentTime() {
        return System.currentTimeMillis();
    }
}
