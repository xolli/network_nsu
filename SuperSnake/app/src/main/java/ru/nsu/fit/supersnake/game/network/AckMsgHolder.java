package ru.nsu.fit.supersnake.game.network;

import ru.nsu.fit.supersnake.game.Field;
import ru.nsu.fit.supersnake.game.message.MessageSnakesProto;

public class AckMsgHolder {
    private Field field;
    private MapSenderId mapSenderId;

    public AckMsgHolder (Field field, MapSenderId mapSenderId) {
        this.field = field;
        this.mapSenderId = mapSenderId;
    }

    public void hold(MessageSnakesProto message) {
        mapSenderId.addSenderId(message.getIp(), message.getPort(), message.getMessage().getSenderId());
        field.setMyId(message.getMessage().getReceiverId());
        System.out.println("Get confirm: " + message.getMessage().getMsgSeq());
        NotConfirmedMessages.getInstance().removeNotConfirmedMessage(message.getMessage().getMsgSeq());
    }
}
