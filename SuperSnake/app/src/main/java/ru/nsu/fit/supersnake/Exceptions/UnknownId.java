package ru.nsu.fit.supersnake.Exceptions;

public class UnknownId extends RuntimeException {
    public UnknownId() {
        super("Unknown receiver's id");
    }
}
