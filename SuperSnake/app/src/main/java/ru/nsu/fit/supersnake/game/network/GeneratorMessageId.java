package ru.nsu.fit.supersnake.game.network;

public class GeneratorMessageId {
    private long globalId;

    private GeneratorMessageId() {
        globalId = 0;
    }

    private static class GeneratorMessageIdHolder {
        private final static GeneratorMessageId instance = new GeneratorMessageId();
    }

    public static GeneratorMessageId getInstance() {
        return GeneratorMessageIdHolder.instance;
    }

    public long getNewId() {
        synchronized (this) {
            ++globalId;
        }
        return globalId;
    }
}
