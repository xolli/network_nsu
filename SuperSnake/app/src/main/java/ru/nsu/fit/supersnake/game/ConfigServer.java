package ru.nsu.fit.supersnake.game;

import android.content.Context;
import android.content.SharedPreferences;

import ru.nsu.fit.supersnake.ui.SetOptionsActivity;

public class ConfigServer {
    private int width;
    private int height;
    private int foodStatic;
    private float foodPerPlayer;
    private int stateDelayMs;
    private float deadFoodProb;
    private int pingDelayMs;
    private int nodeTimeoutMs;

    public ConfigServer(Context context) {
        SharedPreferences options = context.getSharedPreferences(SetOptionsActivity.PREFS_NAME, 0);
        width = options.getInt("width", 40);
        height = options.getInt("height", 40);
        foodStatic = options.getInt("foodStatic", 1);
        foodPerPlayer = options.getFloat("foodPerPlayer", 1);
        stateDelayMs = options.getInt("stateDelayMs", 1000);
        deadFoodProb = options.getFloat("deadFoodProb", (float) 0.1);
        pingDelayMs = options.getInt("pingDelayMs", 100);
        nodeTimeoutMs = options.getInt("nodeTimeoutMs", 800);
    }

    public ConfigServer(int width, int height, int foodStatic, float foodPerPlayer, int stateDelayMs, float deadFoodProb, int pingDelayMs, int nodeTimeoutMs) {
        this.width = width;
        this.height = height;
        this.foodStatic = foodStatic;
        this.foodPerPlayer = foodPerPlayer;
        this.stateDelayMs = stateDelayMs;
        this.deadFoodProb = deadFoodProb;
        this.pingDelayMs = pingDelayMs;
        this.nodeTimeoutMs = nodeTimeoutMs;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getFoodStatic() {
        return foodStatic;
    }

    public float getFoodPerPlayer() {
        return foodPerPlayer;
    }

    public int getStateDelayMs() {
        return stateDelayMs;
    }

    public float getDeadFoodProb() {
        return deadFoodProb;
    }

    public int getPingDelayMs() {
        return pingDelayMs;
    }

    public int getNodeTimeoutMs() {
        return nodeTimeoutMs;
    }
}
