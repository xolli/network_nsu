package ru.nsu.fit.supersnake.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ru.nsu.fit.supersnake.R;

public class SetOptionsActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "options";

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_options_activity);
        displayOptions();
    }

    private void displayOptions() {
        SharedPreferences options = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        displayWidth(options);
        displayHeight(options);
        displayFoodStatic(options);
        displayFoodPerPlayer(options);
        displayStateDelayMs(options);
        displayDeadFoodProb(options);
        displayPingDelayMs(options);
        displayNodeTimeoutMs(options);
        displayUsername(options);
    }

    @SuppressLint("SetTextI18n")
    private void displayWidth(SharedPreferences options) {
        TextView textEditWidth = findViewById(R.id.editWidth);
        textEditWidth.setText(String.valueOf(options.getInt("width", 30)));
    }

    @SuppressLint("SetTextI18n")
    private void displayHeight(SharedPreferences options) {
        TextView textEditHeight = findViewById(R.id.editHeight);
        textEditHeight.setText(String.valueOf(options.getInt("height", 40)));
    }

    @SuppressLint("SetTextI18n")
    private void displayFoodStatic(SharedPreferences options) {
        TextView textFoodStatic = findViewById(R.id.editFoodStatic);
        textFoodStatic.setText(String.valueOf(options.getInt("foodStatic", 1)));
    }

    @SuppressLint("SetTextI18n")
    private void displayFoodPerPlayer(SharedPreferences options) {
        TextView textFoodPerPlayer = findViewById(R.id.editFoodPerPlayer);
        textFoodPerPlayer.setText(String.valueOf(options.getFloat("foodPerPlayer", 1)));
    }

    @SuppressLint("SetTextI18n")
    private void displayStateDelayMs(SharedPreferences options) {
        TextView textStateDelayMs = findViewById(R.id.editStateDelayMs);
        textStateDelayMs.setText(String.valueOf(options.getInt("stateDelayMs", 1000)));
    }

    @SuppressLint("SetTextI18n")
    private void displayDeadFoodProb(SharedPreferences options) {
        TextView textDeadFoodProb = findViewById(R.id.editDeadFoodProb);
        textDeadFoodProb.setText(String.valueOf(options.getFloat("deadFoodProb", (float) 0.1)));
    }

    @SuppressLint("SetTextI18n")
    private void displayPingDelayMs(SharedPreferences options) {
        TextView textPingDelayMs = findViewById(R.id.editPingDelayMs);
        textPingDelayMs.setText(String.valueOf(options.getInt("pingDelayMs", 100)));
    }

    @SuppressLint("SetTextI18n")
    private void displayNodeTimeoutMs(SharedPreferences options) {
        TextView textNodeTimeoutMs = findViewById(R.id.editNodeTimeoutMs);
        textNodeTimeoutMs.setText(String.valueOf(options.getInt("nodeTimeoutMs", 800)));
    }

    @SuppressLint("SetTextI18n")
    private void displayUsername(SharedPreferences options) {
        TextView textUsername = findViewById(R.id.editUsername);
        textUsername.setText(String.valueOf(options.getString("username", "user")));
    }

    public void saveOptions(View view) {
        SharedPreferences options = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = options.edit();
        try {
            setWidth(editor);
            setHeight(editor);
            setFoodStatic(editor);
            setFoodPerPlayer(editor);
            setStateDelayMs(editor);
            setDeadFoodProb(editor);
            setPingDelayMs(editor);
            setNodeTimeoutMs(editor);
            setUsername(editor);
        } catch (NumberFormatException ex) {
            Toast.makeText(this, ex.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        } finally {
            editor.apply();
        }
    }

    private void setWidth(SharedPreferences.Editor editor) {
        EditText editWidth = findViewById(R.id.editWidth);
        String width = editWidth.getText().toString();
        editor.putInt("width", Integer.parseInt(width));
    }

    private void setHeight(SharedPreferences.Editor editor) {
        EditText editHeight = findViewById(R.id.editHeight);
        String height = editHeight.getText().toString();
        editor.putInt("height", Integer.parseInt(height));
    }

    private void setFoodStatic(SharedPreferences.Editor editor) {
        EditText editFoodStatic = findViewById(R.id.editFoodStatic);
        String foodStatic = editFoodStatic.getText().toString();
        editor.putInt("foodStatic", Integer.parseInt(foodStatic));
    }

    private void setFoodPerPlayer(SharedPreferences.Editor editor) {
        EditText editFoodPerPlayer = findViewById(R.id.editFoodPerPlayer);
        String foodPerPlayer = editFoodPerPlayer.getText().toString();
        editor.putFloat("foodPerPlayer", Float.parseFloat(foodPerPlayer));
    }

    private void setStateDelayMs(SharedPreferences.Editor editor) {
        EditText editStateDelayMs = findViewById(R.id.editStateDelayMs);
        String stateDelayMs = editStateDelayMs.getText().toString();
        editor.putInt("stateDelayMs", Integer.parseInt(stateDelayMs));
    }

    private void setDeadFoodProb(SharedPreferences.Editor editor) {
        EditText editDeadFoodProb = findViewById(R.id.editDeadFoodProb);
        String deadFoodProb = editDeadFoodProb.getText().toString();
        editor.putFloat("deadFoodProb", Float.parseFloat(deadFoodProb));
    }

    private void setPingDelayMs(SharedPreferences.Editor editor) {
        EditText editPingDelayMs = findViewById(R.id.editPingDelayMs);
        String pingDelayMs = editPingDelayMs.getText().toString();
        editor.putInt("pingDelayMs", Integer.parseInt(pingDelayMs));
    }

    private void setNodeTimeoutMs(SharedPreferences.Editor editor) {
        EditText editNodeTimeoutMs = findViewById(R.id.editNodeTimeoutMs);
        String nodeTimeoutMs = editNodeTimeoutMs.getText().toString();
        editor.putInt("nodeTimeoutMs", Integer.parseInt(nodeTimeoutMs));
    }

    private void setUsername(SharedPreferences.Editor editor) {
        EditText editUsername = findViewById(R.id.editUsername);
        String username = editUsername.getText().toString();
        editor.putString("username", username);
    }
}
