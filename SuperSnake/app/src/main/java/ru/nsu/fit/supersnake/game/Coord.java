package ru.nsu.fit.supersnake.game;

public class Coord {
    private int x;
    private int y;

    public Coord(Coord coord) {
        x = coord.getX();
        y = coord.getY();
    }

    public Coord(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void incrementY() {
        ++y;
    }

    public void decrementY() {
        --y;
    }

    public void incrementX() {
        ++x;
    }

    public void decrementX() {
        --x;
    }

    public void add (Coord other) {
        x += other.getX();
        y += other.getY();
    }

    public void comeField(int fieldWidth, int fieldHeight) {
        if (x < 0) {
            x = fieldWidth - 1;
        } else if (x >= fieldWidth) {
            x = 0;
        }
        if (y < 0) {
            y = fieldHeight - 1;
        } else if (y >= fieldHeight) {
            y = 0;
        }
    }

    // https://stackoverflow.com/questions/8180430/how-to-override-equals-method-in-java
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != this.getClass()) {
            return false;
        }
        final Coord other = (Coord) obj;
        return other.getX() == x && other.getY() == y;
    }

    @Override
    public int hashCode() {
        return x * 1000 + y;
    }
}
