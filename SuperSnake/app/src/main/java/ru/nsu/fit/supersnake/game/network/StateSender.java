package ru.nsu.fit.supersnake.game.network;

import ru.nsu.fit.supersnake.game.Field;
import ru.nsu.fit.supersnake.game.Converter;
import ru.nsu.fit.supersnake.game.NodeRole;
import ru.nsu.fit.supersnake.game.Player;

public class StateSender implements Runnable {

    private final Field field;
    private final Sender sender;

    public StateSender(Field field, Sender sender) {
        this.field = field;
        this.sender = sender;
    }

    @Override
    public void run() {
        try {
            while (true) {
                field.waitUpdate();
                field.startRead();
                for (Player player : field.getPlayers()) {
                    if (player.getNodeRole() != NodeRole.MASTER && player.getNodeRole() != NodeRole.VIEWER) {
                        sender.sendMessageWithConfirm(Converter.createStateMsg(field, player.getIp(), player.getPort()));
                    }
                }
                field.stopRead();
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }


}
