package ru.nsu.fit.supersnake.game.network;

import java.util.HashMap;

import ru.nsu.fit.supersnake.Exceptions.UnknownId;

public class MapSenderId {
    private class PairIpPort {
        private String ip;
        private int port;

        public PairIpPort(String ip, int port) {
            this.ip = ip;
            this.port = port;
        }

        public String getIp() {
            return ip;
        }

        public int getPort() {
            return port;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            PairIpPort that = (PairIpPort) o;

            if (port != that.port) return false;
            return ip.equals(that.ip);
        }

        @Override
        public int hashCode() {
            int result = ip.hashCode();
            result = 31 * result + port;
            return result;
        }
    }
    private final HashMap<PairIpPort, Integer> data = new HashMap<>();

    public void addSenderId(String ip, int port, int id) {
        synchronized (data) {
            data.put(new PairIpPort(ip, port), id);
        }
    }

    public int getId (String ip, int port) {
        synchronized (data) {
            if (!data.containsKey(new PairIpPort(ip, port))) {
                throw new UnknownId();
            }
            return data.get(new PairIpPort(ip, port));
        }
    }
}
