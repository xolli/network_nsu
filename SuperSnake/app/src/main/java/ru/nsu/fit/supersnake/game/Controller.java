package ru.nsu.fit.supersnake.game;

import me.ippolitov.fit.snakes.SnakesProto;
import ru.nsu.fit.supersnake.game.network.Sender;

// лучше было бы создать Controller и от него унаследовать ControllerServer и ControllerClient
public class Controller {
    private NodeRole role;
    private GameServer server;
    private Player player;

    private Sender sender;
    private String destinationIp;
    private int destinationPort;


    public Controller(GameServer server, Player player) {
        role = NodeRole.MASTER;
        this.server = server;
        this.player = player;
    }

    public Sender getSender() {
        return sender;
    }

    public void setServerMode(GameServer server, Player player) {
        role = NodeRole.MASTER;
        this.server = server;
        this.player = player;
    }

    private static class ControllerHolder {
        private final static Controller instance = new Controller();
    }

    private Controller() {
        role = NodeRole.NORMAL;
    }

    public static Controller getInstance() {
        return ControllerHolder.instance;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public void setDestination(String destinationIp, int destinationPort) {
        this.destinationIp = destinationIp;
        this.destinationPort = destinationPort;
    }

    public void rotate(SnakesProto.Direction newDirection) {
        if (role == NodeRole.MASTER) {
            if (server == null) {
                throw new NullPointerException("Server is null =(");
            }
            server.rotateSnake(player.getId(), newDirection);
        } else if (sender != null && destinationIp != null) {
            System.out.println("send rotate");
            sender.sendMessageWithConfirm(Converter.createSnakeStateMessage(newDirection, destinationIp, destinationPort));
        }
    }

    public void rotatePlayer (SnakesProto.Direction newDirection, int playerId) {
        if (role == NodeRole.MASTER) {
            if (server == null) {
                throw new NullPointerException("Server is null =(");
            }
            server.rotateSnake(playerId, newDirection);
        }
    }
}
