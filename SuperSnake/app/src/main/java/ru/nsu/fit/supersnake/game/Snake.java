package ru.nsu.fit.supersnake.game;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

import me.ippolitov.fit.snakes.SnakesProto;
import ru.nsu.fit.supersnake.Exceptions.WrongDirection;

public class Snake {
    private int playerId;
    private final ArrayList<Coord> points;
    private SnakesProto.Direction headDirection;
    private final ConfigServer config;
    private SnakesProto.GameState.Snake.SnakeState state;

    public enum Bite {
        NO_BITE,
        BITE_HEAD,
        BITE_BODY;
    }

/*    public enum Direction {
        UP,     // Вверх (в отрицательном направлении оси y)
        DOWN,   // Вниз (в положительном направлении оси y)
        LEFT,   // Влево (в отрицательном направлении оси x)
        RIGHT;  // Вправо (в положительном направлении оси x)
    }*/

    public Snake(int playerId, ArrayList<Coord> points, SnakesProto.Direction headDirection, ConfigServer config,
                 SnakesProto.GameState.Snake.SnakeState state) {
        this.playerId = playerId;
        this.points = points;
        this.headDirection = headDirection;
        this.config = config;
        this.state = state;
    }

    public int getPlayerId() {
        return playerId;
    }

    public ArrayList<Coord> getPoints() {
        return points;
    }

    public SnakesProto.Direction getHeadDirection() {
        return headDirection;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public void setHeadDirection(SnakesProto.Direction headDirection) {
        this.headDirection = headDirection;
    }

    public void move() {
        synchronized (points) {
            if (headDirection == SnakesProto.Direction.UP) {
                goUp();
            } else if (headDirection == SnakesProto.Direction.DOWN) {
                goDown();
            } else if (headDirection == SnakesProto.Direction.LEFT) {
                goLeft();
            } else if (headDirection == SnakesProto.Direction.RIGHT) {
                goRight();
            }
        }
    }

    private void goUp() {
        if (points.get(1).equals(new Coord(0, -1))) {
            throw new WrongDirection();
        }
        points.remove(points.size() - 1);
        Coord head = new Coord(points.get(0));
        head.decrementY();
        if (head.getY() < 0) {
            head.setY(config.getHeight() - 1);
        }
        points.remove(0);
        points.add(0, new Coord(0, +1));
        points.add(0, head);
    }

    private void goDown() {
        if (points.get(1).equals(new Coord(0, +1))) {
            throw new WrongDirection();
        }
        points.remove(points.size() - 1);
        Coord head = new Coord(points.get(0));
        head.incrementY();
        if (head.getY() >= config.getHeight()) {
            head.setY(0);
        }
        points.remove(0);
        points.add(0, new Coord(0, -1));
        points.add(0, head);
    }

    private void goLeft() {
        if (points.get(1).equals(new Coord(-1, 0))) {
            throw new WrongDirection();
        }
        points.remove(points.size() - 1);
        Coord head = new Coord(points.get(0));
        head.decrementX();
        if (head.getX() < 0) {
            head.setX(config.getWidth() - 1);
        }
        points.remove(0);
        points.add(0, new Coord(+1, 0));
        points.add(0, head);
    }

    private void goRight() {
        if (points.get(1).equals(new Coord(+1, 0))) {
            throw new WrongDirection();
        }
        points.remove(points.size() - 1);
        Coord head = new Coord(points.get(0));
        head.incrementX();
        if (head.getX() >= config.getWidth()) {
            head.setX(0);
        }
        points.remove(0);
        points.add(0, new Coord(-1, 0));
        points.add(0, head);
    }

    public Coord getHead() {
        synchronized (points) {
            return points.get(0);
        }
    }

    /**
     * @param other other Snake
     * @return this bite other?
     */
    public Bite proveBiting(Snake other, int fieldWidth, int fieldHeight) {
        Coord otherCoord = new Coord (other.getHead());
        if (otherCoord.equals(getHead())) {
            return Bite.BITE_HEAD;
        }
        for (int i = 1; i < other.getPoints().size(); ++i) {
            otherCoord.add(other.getPoints().get(i));
            otherCoord.comeField(fieldWidth, fieldHeight);
            if (otherCoord.equals(getHead())) {
                return Bite.BITE_BODY;
            }
        }
        return Bite.NO_BITE;
    }

    public boolean proveSelfIntersection(int width, int height) {
        Coord head = getHead();
        Coord bodyCoord = new Coord(head);
        for (int i = 1; i < points.size(); ++i) {
            bodyCoord.add(points.get(i));
            bodyCoord.comeField(width, height);
            if (bodyCoord.equals(head)) {
                return true;
            }
        }
        return false;
    }

    public void grow() {
        points.add(new Coord(points.get(points.size() - 1)));
    }

    public SnakesProto.GameState.Snake.SnakeState getState() {
        return state;
    }

    public boolean isZombie() {
        return state == SnakesProto.GameState.Snake.SnakeState.ZOMBIE;
    }

    public void setZombie() {
        state = SnakesProto.GameState.Snake.SnakeState.ZOMBIE;
    }

    public boolean isAlive() {
        return state == SnakesProto.GameState.Snake.SnakeState.ALIVE;
    }

    public boolean validateDirection (SnakesProto.Direction newDirection) {
        synchronized (points) {
            return  !(newDirection == SnakesProto.Direction.UP    && points.get(1).equals(new Coord(0, -1))) &&
                    !(newDirection == SnakesProto.Direction.DOWN  && points.get(1).equals(new Coord(0, +1))) &&
                    !(newDirection == SnakesProto.Direction.RIGHT && points.get(1).equals(new Coord(1, 0))) &&
                    !(newDirection == SnakesProto.Direction.LEFT  && points.get(1).equals(new Coord(-1, 0)));
        }
    }
}
