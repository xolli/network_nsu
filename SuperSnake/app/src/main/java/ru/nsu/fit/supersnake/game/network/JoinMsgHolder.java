package ru.nsu.fit.supersnake.game.network;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import ru.nsu.fit.supersnake.game.Converter;
import ru.nsu.fit.supersnake.game.Field;
import ru.nsu.fit.supersnake.game.NodeRole;
import ru.nsu.fit.supersnake.game.Player;
import ru.nsu.fit.supersnake.game.StoppedRunnable;
import ru.nsu.fit.supersnake.game.message.MessageSnakesProto;

public class JoinMsgHolder extends StoppedRunnable {
    private final Field field;
    private final ArrayList<MessageSnakesProto> queue = new ArrayList<>();
    private final Sender sender;
    private final MapSenderId mapSenderId;

    public JoinMsgHolder (Field field, Sender sender, MapSenderId mapSenderId) {
        super();
        this.field = field;
        this.sender = sender;
        this.mapSenderId = mapSenderId;
    }

    @Override
    public void run() {
        try {
            lock.lock();
            while (!stopped.get()) {
                for (MessageSnakesProto message : queue) {
                    System.out.println("Hold join message");
                    Player addedPlayer = field.addPlayer(message.getMessage().getJoin().getName(), message.getIp(), message.getPort(), NodeRole.NORMAL);
                    sender.sendMessageWithoutConfirm(Converter.createAckMsgNewMsgSeq(message.getIp(), message.getPort(), field.getMyId(), addedPlayer.getId()));
                    if (addedPlayer == null) { // fix, add hold NodeRole
                        // send errorMsg
                    } else if (!field.haveDeputy()) {
                        addedPlayer.setNodeRole(NodeRole.DEPUTY);
                        sender.sendMessageWithConfirm(Converter.createRoleChangeMessage(NodeRole.MASTER,
                                NodeRole.DEPUTY, message.getIp(), message.getPort(), field.getMyId(), addedPlayer.getId()));
                    }
                    mapSenderId.addSenderId(message.getIp(), message.getPort(), addedPlayer.getId());
                }
                queue.clear();
                newEvent.await();
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void hold(MessageSnakesProto message) {
        lock.lock();
        queue.add(message);
        newEvent.signalAll();
        lock.unlock();
    }
}
