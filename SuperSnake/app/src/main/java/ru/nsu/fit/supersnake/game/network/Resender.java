package ru.nsu.fit.supersnake.game.network;

import java.util.HashMap;

import ru.nsu.fit.supersnake.game.Field;
import ru.nsu.fit.supersnake.game.message.InfoMessage;

public class Resender implements Runnable {
    private final NotConfirmedMessages notConfirmedMessages = NotConfirmedMessages.getInstance();
    private final Sender sender;
    private final Field field;

    public Resender(Sender sender, Field field) {
        this.sender = sender;
        this.field = field;
    }

    @Override
    public void run() {
        try {
            while (true) {
                while (field.getConfig() == null) {
                    field.waitUpdate();
                }
                Thread.sleep(field.getConfig().getPingDelayMs());
                HashMap<Long, InfoMessage> copyNotConfirmed;
                synchronized (notConfirmedMessages) {
                    copyNotConfirmed = new HashMap<>(notConfirmedMessages);
                }
                for (Long msgSeq : copyNotConfirmed.keySet()) {
                    if (currentTime() - copyNotConfirmed.get(msgSeq).getTimeSend() >= field.getConfig().getPingDelayMs()) {
                        System.out.println("Resender send message : " + copyNotConfirmed.get(msgSeq).getMessage().type);
                        sender.sendMessageWithoutConfirm(copyNotConfirmed.get(msgSeq).getMessage());
                        copyNotConfirmed.get(msgSeq).resend();
                    }
                }
                copyNotConfirmed.clear();
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private long currentTime() {
        return System.currentTimeMillis();
    }
}
