package ru.nsu.fit.supersnake.game.message;

import java.util.HashSet;

import ru.nsu.fit.supersnake.game.ConfigServer;
import ru.nsu.fit.supersnake.game.Player;

public class AnnouncementMsg {
    private final HashSet<Player> players;
    private final ConfigServer config;
    private final String masterIp;
    private final int masterPort;

    public AnnouncementMsg(HashSet<Player> players, ConfigServer config, String masterIp, int masterPort) {
        this.players = players;
        this.config = config;
        this.masterIp = masterIp;
        this.masterPort = masterPort;
    }

    public HashSet<Player> getPlayers() {
        return players;
    }

    public ConfigServer getConfig() {
        return config;
    }

    public String getMasterIp() {
        return masterIp;
    }

    public int getMasterPort() {
        return masterPort;
    }

    @Override
    public int hashCode() {
        return masterIp.hashCode() + masterPort;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || other.getClass() != this.getClass()) {
            return false;
        }
        AnnouncementMsg otherMsg = (AnnouncementMsg)other;
        return otherMsg.getMasterIp().equals(masterIp) && otherMsg.masterPort == masterPort;
    }
}
