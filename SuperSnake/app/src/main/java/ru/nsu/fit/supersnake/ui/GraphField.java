package ru.nsu.fit.supersnake.ui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.SparseIntArray;

import java.util.Collection;

import ru.nsu.fit.supersnake.game.Coord;
import ru.nsu.fit.supersnake.game.Field;
import ru.nsu.fit.supersnake.game.Player;
import ru.nsu.fit.supersnake.game.Snake;

public class GraphField {
    private final Canvas canvas;
    private final int width;
    private final int height;

    private enum CellType {
        SNAKE_HEAD,
        SNAKE_HORIZONTAL,
        SNAKE_VERTICAL,
        FOOD;
    }

    public GraphField (Canvas canvas, int width, int height) {
        this.canvas = canvas;
        this.width = width;
        this.height = height;
    }

    private int getColor(Snake snake) {
        if (snake.isAlive()) {
            return Color.BLUE;
        } else {
            return Color.GREEN;
        }
    }

    public void drowField (Field field) {
        for (Snake snake : field.getSnakes()) {
            Coord snakeCoord = new Coord(snake.getHead());
            drawCell(CellType.SNAKE_HEAD, snakeCoord, getColor(snake));
            for (int i = 1; i < snake.getPoints().size(); ++i) {
                snakeCoord.add(snake.getPoints().get(i));
                snakeCoord.comeField(width, height);
                if (snake.getPoints().get(i).equals(new Coord(-1, 0)) || snake.getPoints().get(i).equals(new Coord(+1, 0))) {
                    drawCell(CellType.SNAKE_HORIZONTAL, snakeCoord, getColor(snake));
                } else {
                    drawCell(CellType.SNAKE_VERTICAL, snakeCoord, getColor(snake));
                }
            }
            for (Coord food : field.getFoods()) {
                drawCell(CellType.FOOD, food, Color.RED);
            }
        }
    }

    private int maxLengthPlayer(Collection<Player> players) {
        int result = -1;
        for (Player player : players) {
            if (player.toString().length() > result) {
                result = player.toString().length();
            }
        }
        return result;
    }

    public void writeInfo(Field field) {
        int fieldHeightPx = (int) Math.ceil(canvas.getHeight() / 3.0);
        int fieldWidthPx = canvas.getWidth();
        field.startRead();
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        float textSize = Math.min(fieldWidthPx / maxLengthPlayer(field.getPlayers()), fieldHeightPx / field.getPlayersMap().size());
        paint.setTextSize(textSize);
        float currentY = (float) (2.0 * canvas.getHeight() / 3.0) + textSize;
        for (Player player : field.getPlayers()) {
            canvas.drawText(player.toString(), 25, currentY, paint);
            currentY += textSize;
        }
        field.stopRead();
    }

    private void drawCell(CellType cellType, Coord coord, int color) {
        int fieldHeightPx = (int) Math.ceil(2 * canvas.getHeight() / 3.0);
        int fieldWidthPx = canvas.getWidth();
        Paint paint = new Paint();
        paint.setColor(color);
        int cellWidth = fieldWidthPx / width;
        int cellHeight = fieldHeightPx / height;
        if (cellType == CellType.SNAKE_HEAD) {
            int x = cellWidth * coord.getX() + cellWidth / 2;
            int y = cellHeight * coord.getY() + cellHeight / 2;
            canvas.drawCircle(x, y, Math.min(fieldHeightPx / height, fieldWidthPx / width) / 2, paint);
        } else if (cellType == CellType.SNAKE_HORIZONTAL) {
            int leftX = cellWidth * coord.getX();
            int rightX = cellWidth * (coord.getX() + 1);
            int midCellY = cellHeight * coord.getY() + cellHeight / 2;
            canvas.drawRect(leftX, midCellY + cellHeight / 6, rightX, midCellY - cellHeight / 6, paint);
        } else if (cellType == CellType.SNAKE_VERTICAL) {
            int topY = cellHeight * coord.getY();
            int bottomY = cellHeight * (coord.getY() + 1);
            int midCellX = cellWidth * coord.getX() + cellWidth / 2;
            canvas.drawRect(midCellX - cellWidth / 6, topY, midCellX + cellWidth / 6, bottomY, paint);
        } else if (cellType == CellType.FOOD) {
            int x = cellWidth * coord.getX() + cellWidth / 2;
            int y = cellHeight * coord.getY() + cellHeight / 2;
            paint.setColor(Color.RED);
            canvas.drawCircle(x, y, Math.min(fieldHeightPx / height, fieldWidthPx / width) / 2, paint);
        }
    }

}
