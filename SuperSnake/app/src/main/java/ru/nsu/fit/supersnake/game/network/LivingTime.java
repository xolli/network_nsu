package ru.nsu.fit.supersnake.game.network;

import java.util.HashMap;

public class LivingTime extends HashMap<Integer, Long> {

    private LivingTime() {
        super();
    }

    private static class LivingTimeHolder {
        private final static LivingTime instance = new LivingTime();
    }

    public static LivingTime getInstance() {
        return LivingTimeHolder.instance;
    }

    public void updateSender(Integer id) {
        put(id, currentTime());
    }

    private Long currentTime() {
        return System.currentTimeMillis();
    }
}
