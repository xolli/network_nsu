package ru.nsu.fit.supersnake.Exceptions;

public class WrongDirection extends RuntimeException {
    public WrongDirection() {
        super("Wrong snake's direction");
    }

    public WrongDirection(int playerId) {
        super("Wrong snake's direction with id = " + playerId);
    }
}
