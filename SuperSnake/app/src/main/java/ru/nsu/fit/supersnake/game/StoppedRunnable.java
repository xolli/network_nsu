package ru.nsu.fit.supersnake.game;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public abstract class StoppedRunnable implements Runnable {
    protected final AtomicBoolean stopped = new AtomicBoolean(false);
    protected final ReentrantLock lock = new ReentrantLock();
    protected final Condition newEvent = lock.newCondition();

    public void stop() {
        lock.lock();
        stopped.set(true);
        newEvent.signalAll();
        lock.unlock();
    }

}
