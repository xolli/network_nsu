package ru.nsu.fit.supersnake.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ru.nsu.fit.supersnake.R;
import ru.nsu.fit.supersnake.game.NodeRole;
import ru.nsu.fit.supersnake.game.Player;
import ru.nsu.fit.supersnake.game.message.AnnouncementGames;
import ru.nsu.fit.supersnake.game.message.AnnouncementMsg;

// https://metanit.com/java/android/5.11.php
public class GameDataAdapter extends RecyclerView.Adapter<GameDataAdapter.ViewHolder> {
    private final OnGameClickListener onGameClickListener;
    private final LayoutInflater inflater;
    private final AnnouncementGames games;

    public GameDataAdapter(Context context, AnnouncementGames games, OnGameClickListener onGameClickListener) {
        this.games = games;
        this.inflater = LayoutInflater.from(context);
        this.onGameClickListener = onGameClickListener;
    }

    @NonNull
    @Override
    public GameDataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AnnouncementMsg game = games.getGame(position);
        holder.ip.setText(String.format("%s:%s (master: %s)", game.getMasterIp(), game.getMasterPort(), getMasterName(game)));
    }

    @Override
    public int getItemCount() {
        return games.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView ip;

        public ViewHolder(View view){
            super(view);
            ip = (TextView) view.findViewById(R.id.ip);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AnnouncementMsg game = games.getGame(getLayoutPosition());
                    onGameClickListener.onGameClick(game);
                }
            });
        }
    }

    private String getMasterName(AnnouncementMsg game) {
        for (Player player : game.getPlayers()) {
            if (player.getNodeRole() == NodeRole.MASTER) {
                return player.getName();
            }
        }
        return "Unknown";
    }

    public interface OnGameClickListener {
        void onGameClick(AnnouncementMsg game);
    }
}
