package ru.nsu.fit.supersnake.ui;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import ru.nsu.fit.supersnake.R;


public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /** Called when the user taps the Options button */
    public void setOptions(View view) {
        Intent intent = new Intent(this, SetOptionsActivity.class);
        startActivity(intent);
    }

    /** Called when the user taps the Start New Game button */
    public void startNewGame(View view) {
        Intent intent = new Intent(this, StartNewGameActivity.class);
        startActivity(intent);
    }

    /** Called when the user taps the Start New Game button */
    public void joinToExistGame(View view) {
        Intent intent = new Intent(this, JoinGamesActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
