package ru.nsu.fit.supersnake.game.network;

import android.content.Context;
import android.net.wifi.WifiManager;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.concurrent.atomic.AtomicBoolean;

import ru.nsu.fit.supersnake.Exceptions.IncorrectAnnoncedMessage;
import ru.nsu.fit.supersnake.Exceptions.IncorrectMasterPort;
import ru.nsu.fit.supersnake.game.Converter;
import ru.nsu.fit.supersnake.game.message.AnnouncementGames;
import ru.nsu.fit.supersnake.game.message.AnnouncementMsg;
import ru.nsu.fit.supersnake.ui.ListenerExistsGames;

public class MulticastReceiver implements Runnable {
//    private static final String groupAddress = "239.192.0.4";
    private static final String groupAddress = "224.0.0.1";
    private static final int port = 9192;
    private final byte[] buf = new byte[1024];
    private MulticastSocket socket = null;
    private final AnnouncementGames games;
    private final AtomicBoolean stopReceiving;
    private WifiManager.MulticastLock multicastLock;
    private final Context context;
    private final ListenerExistsGames listenerExistsGames;

    public MulticastReceiver(AnnouncementGames games, Context context, ListenerExistsGames listenerExistsGames) {
        this.games = games;
        this.stopReceiving = new AtomicBoolean(false);
        this.context = context;
        this.listenerExistsGames = listenerExistsGames;
    }

    @Override
    public void run() {
        InetAddress group = null;
        startReceive();
        try {
            socket = new MulticastSocket(port);
            group = InetAddress.getByName(groupAddress);
            socket.joinGroup(group);
            mainCycle();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (group != null) {
                try {
                    socket.leaveGroup(group);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            stopReceive();
            socket.close();
        }
    }

    private void mainCycle() throws IOException {
        AnnouncementMsg msg;
        while (!stopReceiving.get()) {
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            if (stopReceiving.get()) {
                break;
            }
            socket.receive(packet);
            try {
                msg = Converter.convertAnnouncementMsg(packet);
            } catch (IncorrectAnnoncedMessage | IncorrectMasterPort ex) {
                continue;
            }
            listenerExistsGames.newGame(msg);
        }
    }

    // https://stackoverflow.com/questions/13221736/android-device-not-receiving-multicast-package
    // https://stackoverflow.com/questions/20921023/android-can-not-receive-multicast-packet
    // this solution doesn't work on Sony E2303 =(
    private void startReceive() {
        WifiManager wifi = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        multicastLock = wifi.createMulticastLock("multicastLock");
        multicastLock.setReferenceCounted(true);
        multicastLock.acquire();
    }

    private void stopReceive() {
        if (multicastLock != null) {
            multicastLock.release();
            multicastLock = null;
        }
    }

    public void stop() {
        stopReceiving.set(true);
    }
}
