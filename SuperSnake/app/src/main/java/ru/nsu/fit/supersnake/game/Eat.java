package ru.nsu.fit.supersnake.game;

public class Eat {
    private Coord coord;

    public Eat (Coord coord) {
        this.coord = coord;
    }

    public Coord getCoord() {
        return coord;
    }
}
