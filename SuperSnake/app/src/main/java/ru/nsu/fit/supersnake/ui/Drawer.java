package ru.nsu.fit.supersnake.ui;

import android.graphics.Canvas;
import android.graphics.Color;

import ru.nsu.fit.supersnake.game.Field;

public class Drawer implements Runnable {
    private final GameView gameView;
    private final Field field;

    public Drawer (GameView gameView, Field field) {
        this.gameView = gameView;
        this.field = field;
    }

    public void drawField() {
        Canvas canvas = gameView.getHolder().lockCanvas();
        if (canvas != null){
            canvas.drawColor(Color.BLACK);
            gameView.drawField(canvas);
            gameView.getHolder().unlockCanvasAndPost(canvas);
        }
    }

    @Override
    public void run() {
        try {
            while (!field.isEmpty()) {
                field.startRead();
                drawField();
                field.stopRead();
                field.waitUpdate();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
