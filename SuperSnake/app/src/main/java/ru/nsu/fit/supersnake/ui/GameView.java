package ru.nsu.fit.supersnake.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;

import androidx.core.view.GestureDetectorCompat;

import me.ippolitov.fit.snakes.SnakesProto;
import ru.nsu.fit.supersnake.game.ConfigServer;
import ru.nsu.fit.supersnake.game.Controller;
import ru.nsu.fit.supersnake.game.Field;
import ru.nsu.fit.supersnake.game.Snake;

@SuppressLint("ViewConstructor")
public class GameView extends SurfaceView {
    private Field field;
    private ConfigServer config;
    private Controller controller;
    private GestureDetectorCompat lSwipeDetector;
    private static final int SWIPE_MIN_DISTANCE = 130;
    private static final int SWIPE_MIN_VELOCITY = 200;

    public GameView(Context context, Field field, ConfigServer config, Controller controller) {
        super(context);
        this.field = field;
        this.config = config;
        this.controller = controller;
        lSwipeDetector = new GestureDetectorCompat(context, new MyGestureListener());
        setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return lSwipeDetector.onTouchEvent(event);
            }
        });
    }

    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener{
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY){
            if (Math.abs(e1.getY() - e2.getY()) > Math.abs(e1.getX() - e2.getX())) {
                if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_MIN_VELOCITY) {
                    controller.rotate(SnakesProto.Direction.DOWN);
                } else if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_MIN_VELOCITY) {
                    controller.rotate(SnakesProto.Direction.UP);
                }
            } else {
                if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_MIN_VELOCITY) {
                    controller.rotate(SnakesProto.Direction.RIGHT);
                } else if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_MIN_VELOCITY) {
                    controller.rotate(SnakesProto.Direction.LEFT);
                }
            }
            return false;
        }
    }


    public void drawField(Canvas canvas) {
        canvas.drawColor(Color.BLACK);
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        GraphField graphField = new GraphField(canvas, config.getWidth(), config.getHeight());
        graphField.drowField(field);
        graphField.writeInfo(field);
    }
}
