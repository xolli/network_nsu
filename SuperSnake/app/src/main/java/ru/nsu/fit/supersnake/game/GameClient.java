package ru.nsu.fit.supersnake.game;

import android.graphics.Canvas;
import android.graphics.Color;

import java.util.concurrent.CompletableFuture;

import me.ippolitov.fit.snakes.SnakesProto;
import ru.nsu.fit.supersnake.ui.GameView;

public class GameClient {
    private final ConfigServer config;
    private final Controller controller;
    private final Player player;

    public GameClient(ConfigServer config, GameView gameView, Controller controller, Player player) {
        this.config = config;
        this.controller = controller;
        this.player = player;
    }

    public void deletePlayer(Integer id) {
    }

    public void rotateSnake(SnakesProto.Direction newDirection) {
//        controller.rotate(newDirection);
    }
}
