package ru.nsu.fit.supersnake.game.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import ru.nsu.fit.supersnake.game.Player;
import ru.nsu.fit.supersnake.game.message.Message;

public class Sender implements Runnable {
    private final DatagramSocket socket;
    private final NotConfirmedMessages notConfirmedMessages;
    private final ArrayList<Message> queueConfirm = new ArrayList<>();
    private final ArrayList<Message> queueWithoutConfirm = new ArrayList<>();
    private final ReentrantLock lock = new ReentrantLock();
    private final Condition newMessage = lock.newCondition();
    private final AtomicBoolean continueSend = new AtomicBoolean(true);

    public Sender(DatagramSocket socket) {
        this.socket = socket;
        notConfirmedMessages = NotConfirmedMessages.getInstance();
    }

    public void sendMessageWithConfirm(Message message) {
        lock.lock();
        try {
            queueConfirm.add(message);
            newMessage.signalAll();
        } finally {
            lock.unlock();
        }
    }

    public void sendMessageWithoutConfirm(Message message) {
        lock.lock();
        try {
            queueWithoutConfirm.add(message);
            newMessage.signalAll();
        } finally {
            lock.unlock();
        }
    }

    private void _sendMessageWithConfirm(Message message) {
        notConfirmedMessages.addNotConfirmedPacket(message);
        _sendMessageWithoutConfirm(message);
    }

    private void _sendMessageWithoutConfirm(Message message) {
        try {
            InetAddress senderAddress = InetAddress.getByName(message.getDestinationIp());
            DatagramPacket newPacket = new DatagramPacket(message.getData(), message.getData().length, senderAddress, message.getDestinationPort());
            socket.send(newPacket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        lock.lock();
        try {
            while (continueSend.get()) {
                newMessage.await();
                for (Message message : queueConfirm) {
                    _sendMessageWithConfirm(message);
                }
                for (Message message : queueWithoutConfirm) {
                    _sendMessageWithoutConfirm(message);
                }
                queueConfirm.clear();
                queueWithoutConfirm.clear();
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void stop() {
        continueSend.set(false);
        lock.lock();
        try {
            newMessage.signal();
        } finally {
            lock.unlock();
        }
    }
}