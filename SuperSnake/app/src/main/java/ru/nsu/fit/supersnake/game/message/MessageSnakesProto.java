package ru.nsu.fit.supersnake.game.message;

import me.ippolitov.fit.snakes.SnakesProto;

public class MessageSnakesProto {
    private SnakesProto.GameMessage message;
    private String ip;
    private int port;

    public MessageSnakesProto(SnakesProto.GameMessage message, String ip, int port) {
        this.message = message;
        this.ip = ip;
        this.port = port;
    }

    public SnakesProto.GameMessage getMessage() {
        return message;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }
}
