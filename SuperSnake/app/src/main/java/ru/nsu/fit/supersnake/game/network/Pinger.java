package ru.nsu.fit.supersnake.game.network;

import ru.nsu.fit.supersnake.game.Field;
import ru.nsu.fit.supersnake.game.Converter;
import ru.nsu.fit.supersnake.game.NodeRole;
import ru.nsu.fit.supersnake.game.Player;

public class Pinger implements Runnable {
    private final Sender sender;
    private final Field field;
    private final String myName;

    public Pinger(Field field, String myName, Sender sender) {
        this.field = field;
        this.myName = myName;
        this.sender = sender;
    }

    @Override
    public void run() {
        LivingTime livingTime = LivingTime.getInstance();
        try {
            synchronized (this) {
                while (true) {
                    while (field.getConfig() == null) {
                        field.waitUpdate();
                    }
                    Thread.sleep(field.getConfig().getPingDelayMs());
                    field.startRead();
                    for (Player player : field.getPlayers()) {
                        if (livingTime.containsKey(player.getId()) && (currentTime() - livingTime.get(player.getId()) < field.getConfig().getPingDelayMs())) {
                            continue;
                        }
                        if (!player.getName().equals(myName) && player.getNodeRole() != NodeRole.VIEWER) {
                            sender.sendMessageWithConfirm(Converter.createPingMsg(player.getIp(), player.getPort()));
                        }
                    }
                    field.stopRead();
                }
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private Long currentTime() {
        return System.currentTimeMillis();
    }
}
