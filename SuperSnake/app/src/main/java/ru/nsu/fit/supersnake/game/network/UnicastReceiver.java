package ru.nsu.fit.supersnake.game.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.atomic.AtomicBoolean;

import me.ippolitov.fit.snakes.SnakesProto;
import ru.nsu.fit.supersnake.Exceptions.UnknownId;
import ru.nsu.fit.supersnake.game.Controller;
import ru.nsu.fit.supersnake.game.Field;
import ru.nsu.fit.supersnake.game.Converter;
import ru.nsu.fit.supersnake.game.message.MessageSnakesProto;

public class UnicastReceiver implements Runnable {
    private final LivingTime livingTime = LivingTime.getInstance();
    private final DatagramSocket socket;
    private final Sender sender;
    private final AtomicBoolean continueWork = new AtomicBoolean(true);
    private final JoinMsgHolder joinHolder;
    private final StateMsgHolder stateMsgHolder;
    private final SteerMsgHolder steerMsgHolder;
    private final AckMsgHolder ackMsgHolder;
    private final RoleChangeMsgHolder roleChangeMsgHolder;
    private final Thread joinHolderThread;
    private final Thread stateMsgHolderThread;
    private final Thread steerMsgHolderThread;
    private final MapSenderId mapSenderId;
    private final Field field;

    public UnicastReceiver(DatagramSocket socket, Field field, Sender sender, Controller controller) {
        this.socket = socket;
        this.sender = sender;
        this.field = field;
        mapSenderId = new MapSenderId();
        joinHolder = new JoinMsgHolder(field, sender, mapSenderId);
        joinHolderThread = new Thread(joinHolder);
        joinHolderThread.start();
        stateMsgHolder = new StateMsgHolder(field, sender);
        stateMsgHolderThread = new Thread(stateMsgHolder);
        stateMsgHolderThread.start();
        steerMsgHolder = new SteerMsgHolder(controller, mapSenderId);
        steerMsgHolderThread = new Thread(steerMsgHolder);
        steerMsgHolderThread.start();
        ackMsgHolder = new AckMsgHolder(field, mapSenderId);
        roleChangeMsgHolder = new RoleChangeMsgHolder(field);
    }

    @Override
    public void run() {
        byte[] buf = new byte[1024];
        try {
            while(continueWork.get()) {
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet);
                MessageSnakesProto message = new MessageSnakesProto(Converter.convertMessage(packet), packet.getAddress().getHostAddress(), packet.getPort());
                switch (message.getMessage().getTypeCase()) {
                    case JOIN: joinHolder.hold(message);
                        System.out.println("get join request message");
                        break;
                    case STATE: stateMsgHolder.hold(message);
                        System.out.println("get state message");
                        break;
                    case ACK: ackMsgHolder.hold(message);
                        System.out.println("get ack message");
                        break;
                    case STEER: steerMsgHolder.hold(message);
                        System.out.println("get steer message");
                    break;
                    case PING: System.out.println("get ping with msgseq: " + message.getMessage().getMsgSeq());
                    break;
                    case ERROR:
                        System.out.println("get error message: " + message.getMessage().getMsgSeq());
                        break;
                    case ROLE_CHANGE: roleChangeMsgHolder.hold(message);
                        System.out.println("get role_change message: " + message.getMessage().getMsgSeq());
                        break;
                    case ANNOUNCEMENT:
                        System.out.println("get annoncement message: " + message.getMessage().getMsgSeq());
                        break;
                }
                synchronized (livingTime) {
                    field.startRead();
                    int senderId = field.getPlayerId(packet.getAddress().getHostAddress(), packet.getPort());
                    if (senderId != -1) {
                        livingTime.updateSender(senderId);
                    }
                    field.stopRead();
                }
                System.out.println("get message with msgSeq: " + message.getMessage().getMsgSeq());
                System.out.println("NotConfirmedMessage: " + NotConfirmedMessages.getInstance());
                if (message.getMessage().getTypeCase() != SnakesProto.GameMessage.TypeCase.ACK) {
                    long msgSeq = message.getMessage().getMsgSeq();
                    sendAckMsg(packet.getAddress().getHostAddress(), packet.getPort(), msgSeq);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void sendAckMsg(String destinationIp, int destinationPort, long msgSeq) {
//        System.out.println("Send ackMsg " + destinationIp + ":" + destinationIp + " msgSeq = " + msgSeq);
        try {
            sender.sendMessageWithoutConfirm(Converter.createAckMsg(destinationIp, destinationPort, msgSeq, field.getMyId(), mapSenderId.getId(destinationIp, destinationPort)));
        } catch (UnknownId ex) {
            ex.printStackTrace();
        }
    }

    public void stop() {
        continueWork.set(false);
        joinHolder.stop();
        stateMsgHolder.stop();
        steerMsgHolder.stop();
        joinHolderThread.interrupt();
        stateMsgHolderThread.interrupt();
        steerMsgHolderThread.interrupt();
    }
}
