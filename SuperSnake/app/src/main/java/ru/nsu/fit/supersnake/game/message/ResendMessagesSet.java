package ru.nsu.fit.supersnake.game.message;

import java.util.HashSet;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ResendMessagesSet {
    private final HashSet<Message> messages = new HashSet<>();
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    public HashSet<Message> getMessages() {
        lock.readLock().lock();
        HashSet<Message> result = new HashSet<>(messages);
        lock.readLock().unlock();
        return result;
    }

    public void addMessage (Message newMessage) {
        lock.writeLock().lock();
        messages.add(newMessage);
        lock.writeLock().unlock();
    }

    public void removeMessage (Message deletedMessage) {
        lock.writeLock().lock();
        messages.remove(deletedMessage);
        lock.writeLock().unlock();
    }

}
