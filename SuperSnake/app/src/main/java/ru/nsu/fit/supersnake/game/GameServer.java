package ru.nsu.fit.supersnake.game;

import android.content.Context;
import android.widget.Toast;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import me.ippolitov.fit.snakes.SnakesProto;
import ru.nsu.fit.supersnake.game.network.Announcer;
import ru.nsu.fit.supersnake.game.network.Sender;


public class GameServer implements Runnable {
    private final ConfigServer config;
    private final ServerInitState initState;
    private final Field field;
    private Thread announcerThread;
    private final AtomicBoolean work = new AtomicBoolean(true);

    public GameServer(ConfigServer config, ServerInitState initState, Field field) {
        this.config = config;
        this.initState = initState;
        this.field = field;
    }

    @Override
    public void run() {
        Announcer announcer;
        try {
            announcer = new Announcer(field, config);
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }
        announcerThread = new Thread(announcer);
        announcerThread.start();
        start();
    }

    private void start() {
        try {
            while (true) {
                Thread.sleep(config.getStateDelayMs());
                field.move();
                if (field.getMyRole() != NodeRole.MASTER) {
                    work.set(false);
                    return;
                }
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } finally {
            announcerThread.interrupt();
        }
    }

    public void rotateSnake(int playerId, SnakesProto.Direction newDirection) {
        if (!work.get()) {
            return;
        }
        for (Snake snake : field.getSnakes()) {
            if (snake.getPlayerId() == playerId && snake.validateDirection(newDirection)) {
                snake.setHeadDirection(newDirection);
                break;
            }
        }
    }
}
