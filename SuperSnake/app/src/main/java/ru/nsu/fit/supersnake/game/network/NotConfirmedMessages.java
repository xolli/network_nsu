package ru.nsu.fit.supersnake.game.network;

import java.util.HashMap;
import java.util.HashSet;

import ru.nsu.fit.supersnake.game.message.InfoMessage;
import ru.nsu.fit.supersnake.game.message.Message;

public class NotConfirmedMessages extends HashMap<Long, InfoMessage> {

    private NotConfirmedMessages() {
        super();
    }

    public void removeReceiver(String ip, int port) {
        synchronized (this) {
            HashSet<Long> deletedMessages = new HashSet<>();
            for (Long msgSeq : keySet()) {
                Message message = get(msgSeq).getMessage();
                if (message.getDestinationIp().equals(ip) && message.getDestinationPort() == port) {
                    deletedMessages.add(msgSeq);
                }
            }
            for (Long msgSeq : deletedMessages) {
                this.remove(msgSeq);
            }
        }
    }

    private static class NotConfirmedMessagesHolder {
        private final static NotConfirmedMessages instance = new NotConfirmedMessages();
    }

    public static NotConfirmedMessages getInstance() {
        return NotConfirmedMessagesHolder.instance;
    }

    public void addNotConfirmedPacket(Message message) {
        System.out.println("Wait confirm: " + message.getMsgSeq());
        synchronized (this) {
            put(message.getMsgSeq(), new InfoMessage(message));
        }
    }

    public void removeNotConfirmedMessage(long msgSeq) {
        synchronized (this) {
            remove(msgSeq);
        }
    }

    public void changeDestination(String newIp, int newPort, String oldIp, int oldPort) {
        synchronized (this) {
            for (InfoMessage info : this.values()) {
                Message message = info.getMessage();
                if (message.getDestinationIp().equals(oldIp) && message.getDestinationPort() == oldPort) {
                    message.setDestination(newIp, newPort);
                }
            }
        }
    }
}
