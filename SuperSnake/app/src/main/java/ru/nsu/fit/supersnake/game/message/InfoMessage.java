package ru.nsu.fit.supersnake.game.message;

public class InfoMessage {
    private final Message message;
    private long timeSend;

    public InfoMessage(Message message) {
        this.message = message;
        timeSend = currentTime();
    }

    public Message getMessage() {
        return message;
    }

    public long getTimeSend() {
        return timeSend;
    }

    public void resend() {
        timeSend = currentTime();
    }

    private long currentTime() {
        return System.currentTimeMillis();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InfoMessage that = (InfoMessage) o;
        return message != null ? message.getMsgSeq() == that.getMessage().getMsgSeq() : that.message == null;
    }

    @Override
    public int hashCode() {
        return (message != null) ? Long.valueOf(message.getMsgSeq()).hashCode() : 0;
    }

    @Override
    public String toString() {
        return message.toString();
    }
}
