package ru.nsu.fit.supersnake.game;

import androidx.annotation.IntegerRes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import me.ippolitov.fit.snakes.SnakesProto;
import ru.nsu.fit.supersnake.game.message.MessageSnakesProto;
import ru.nsu.fit.supersnake.game.network.Sender;

public class Field {
    private int stateOrder;
    private final HashSet<Snake> snakes;
    private final HashSet<Coord> foods;
    private final HashMap<Integer, Player> players;
    private ConfigServer config;
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final Condition updateField  = lock.writeLock().newCondition();
    private final AtomicBoolean initialized  = new AtomicBoolean(false);
    private int myId;

    public Field(ConfigServer config, Player firstPlayer) {
        stateOrder = 0;
        snakes = new HashSet<>();
        foods = new HashSet<>();
        players = new HashMap<>();
        players.put(firstPlayer.getId(), firstPlayer);
        myId = firstPlayer.getId();
        ArrayList<Coord> pointSnake = new ArrayList<>(2);
        pointSnake.add(new Coord(10, 10));
        pointSnake.add(new Coord(0, -1));
        snakes.add(new Snake(0, pointSnake, SnakesProto.Direction.DOWN, config, SnakesProto.GameState.Snake.SnakeState.ALIVE));
        this.config = config;
        synchronized (initialized) {
            initialized.set(true);
            initialized.notifyAll();
        }
    }

    private Field() {
        stateOrder = 0;
        myId = 0;
        snakes = new HashSet<>();
        foods = new HashSet<>();
        players = new HashMap<>();
    }

    public void setMyId(int id) {
        myId = id;
    }

    public int getMyId() {
        return myId;
    }

    public void clear() {
        initialized.set(false);
        stateOrder = 0;
        snakes.clear();
        foods.clear();
        players.clear();
    }

    public void setMasterIp(String ip) {
        lock.writeLock().lock();
        for (Integer playerId : players.keySet()) {
            if (players.get(playerId).getNodeRole() == NodeRole.MASTER) {
                players.get(playerId).setIp(ip);
                break;
            }
        }
        lock.writeLock().unlock();
    }

    public void becomeMaster() {
        lock.writeLock().lock();
        for (Player player : players.values()) {
            if (player.getId() == myId) {
                player.setNodeRole(NodeRole.MASTER);
            }
        }
        for (Integer id : players.keySet()) {
            if (id != myId && players.get(id).getNodeRole() == NodeRole.MASTER) {
                players.remove(id);
                setZombie(id);
                break;
            }
        }
        lock.writeLock().unlock();
    }

    public void setZombie(int id) {
        for (Snake snake : snakes) {
            if (snake.getPlayerId() == id) {
                snake.setZombie();
                return;
            }
        }
    }

    public Player updateDeputy() {
        lock.writeLock().lock();
        try {
            return _updateDeputy();
        } finally {
            lock.writeLock().unlock();
        }
    }

    private Player _updateDeputy() {
        Player deputy = getDeputy();
        for (Player player : players.values()) {
            if (!player.equals(deputy) && player.getNodeRole() != NodeRole.MASTER &&
                    player.getNodeRole() != NodeRole.VIEWER) {
                player.setNodeRole(NodeRole.DEPUTY);
                deputy.setNodeRole(NodeRole.VIEWER);
                return player;
            }
        }
        return null;
    }

    public void infectSnake(Integer id) {
        lock.writeLock().lock();
        try {
            for (Snake snake : snakes) {
                if (snake.getPlayerId() == id) {
                    snake.setZombie();
                    break;
                }
            }
        } finally {
            lock.writeLock().unlock();
        }
    }

    public Integer getPlayerId(String ip, int port) {
        for (Player player : players.values()) {
            if (player.getIp().equals(ip) && player.getPort() == port) {
                return player.getId();
            }
        }
        return -1;
    }

    public void removePlayer(Integer id) {
        lock.writeLock().lock();
        try {
            players.remove(id);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public void setViewer(Integer id) {
        lock.writeLock().lock();
        try {
            Player viewerPlayer = players.get(id);
            if (viewerPlayer != null) {
                viewerPlayer.setNodeRole(NodeRole.VIEWER);
            }
        } finally {
            lock.writeLock().unlock();
        }
    }

    private static class FieldHolder {
        private final static Field emptyInstance = new Field();
    }

    public static Field getEmptyInstance() {
        return FieldHolder.emptyInstance;
    }

    public ConfigServer getConfig() throws InterruptedException {
        synchronized (initialized) {
            while (!initialized.get()) {
                initialized.wait();
            }
        }
        return config;
    }

    public HashSet<Snake> getSnakes() {
        return snakes;
    }

    public HashSet<Coord> getFoods() {
        return foods;
    }

    public void startRead() {
        lock.readLock().lock();
//        System.out.println("Start read");
    }

    public void stopRead() {
        lock.readLock().unlock();
//        System.out.println("Stop read");
    }

    public void waitUpdate() throws InterruptedException {
        lock.writeLock().lock();
        try {
            updateField.await();
        } finally {
            lock.writeLock().unlock();
        }
    }

    public void move() {
        lock.writeLock().lock();
        try {
            for (Snake snake : snakes) {
                snake.move();
            }
            omNomNom();
            killSnakes(findCrossingSnakes());
            generateFood();
            ++stateOrder;
            updateField.signalAll();
        }  finally {
            lock.writeLock().unlock();
        }
    }

    public void update(MessageSnakesProto message) {
        lock.writeLock().lock();
        stateOrder = message.getMessage().getState().getState().getStateOrder();
        snakes.clear();
        for (SnakesProto.GameState.Snake snake : message.getMessage().getState().getState().getSnakesList()) {
            snakes.add(convertSnake(snake));
        }
        foods.clear();
        for (SnakesProto.GameState.Coord food : message.getMessage().getState().getState().getFoodsList()) {
            foods.add(new Coord(food.getX(), food.getY()));
        }
        players.clear();
        for (SnakesProto.GamePlayer player : message.getMessage().getState().getState().getPlayers().getPlayersList()) {
            Player newPlayer = new Player(player.getName(), player.getId(), player.getIpAddress(), player.getPort(), Converter.convertNodeRole(player.getRole()));
            newPlayer.setScore(player.getScore());
            players.put(player.getId(), newPlayer);
        }
        config = Converter.convertConfig(message.getMessage().getState().getState().getConfig());
        synchronized (initialized) {
            initialized.set(true);
            initialized.notifyAll();
        }
        updateField.signalAll();
        lock.writeLock().unlock();
    }

    public boolean initialized() {
        return initialized.get();
    }

    private Snake convertSnake(SnakesProto.GameState.Snake protoSnake) {
        ArrayList<Coord> myPoints = new ArrayList<>();
        for (SnakesProto.GameState.Coord point : protoSnake.getPointsList()) {
            myPoints.add(new Coord(point.getX(), point.getY()));
        }
        return new Snake(protoSnake.getPlayerId(), myPoints, protoSnake.getHeadDirection(), config, protoSnake.getState());
    }

    public int getStateOrder() {
        return stateOrder;
    }

    public Collection<Player> getPlayers() {
        return players.values();
    }

    public boolean haveDeputy() {
        for (Player player : players.values()) {
            if (player.getNodeRole() == NodeRole.DEPUTY) {
                return true;
            }
        }
        return false;
    }

    public Map<Integer, Player> getPlayersMap() {
        return players;
    }

    public boolean isEmpty() {
        return snakes.isEmpty();
    }

    private void omNomNom() {
        HashSet<Coord> eaten = new HashSet<>();
        for (Snake snake : snakes) {
            for (Coord food : foods) {
                if (food.equals(snake.getHead())) {
                    snake.grow();
                    if (players.containsKey(snake.getPlayerId())) {
                        players.get(snake.getPlayerId()).addPoint();
                    }
                    eaten.add(food);
                }
            }
        }
        foods.removeAll(eaten);
    }

    private HashSet<Snake> findCrossingSnakes() {
        HashSet<Snake> deadSnakes = new HashSet<>();
        for (Snake snake : snakes) {
            if (snake.proveSelfIntersection(config.getWidth(), config.getHeight())) {
                deadSnakes.add(snake);
            }
        }
        for (Snake snake1 : snakes) {
            for (Snake snake2 : snakes) {
                if (snake1.equals(snake2)) {
                    continue;
                }
                if (snake1.proveBiting(snake2, config.getWidth(), config.getHeight()) == Snake.Bite.BITE_HEAD) {
                    deadSnakes.add(snake1);
                    deadSnakes.add(snake2);
                } else if (snake1.proveBiting(snake2, config.getWidth(), config.getHeight()) == Snake.Bite.BITE_BODY) {
                    deadSnakes.add(snake1);
                    players.get(snake2.getPlayerId()).addPoint();
                } else if (snake2.proveBiting(snake1, config.getWidth(), config.getHeight()) == Snake.Bite.BITE_BODY) {
                    deadSnakes.add(snake2);
                    players.get(snake1.getPlayerId()).addPoint();
                }
            }
        }
        return deadSnakes;
    }

    private void killSnakes(HashSet<Snake> deadSnakes) {
        for (Snake diedSnake : deadSnakes) {
            cookSnake(diedSnake);
            Player diedPlayer = players.get(diedSnake.getPlayerId());
            if (diedPlayer == null) {
                continue;
            }
            killPlayer(diedPlayer);
        }
        snakes.removeAll(deadSnakes);
    }

    private void killPlayer(Player diedPlayer) {
        diedPlayer.setNodeRole(NodeRole.VIEWER);
        if (diedPlayer.getNodeRole() == NodeRole.DEPUTY) {
            _updateDeputy();
            // send changeNodeRole message to deputy?
        }
    }

    private void cookSnake(Snake diedSnake) {
        Coord snakeCoord = diedSnake.getHead();
        if (generateRandomDouble(0, 1) < config.getDeadFoodProb()) {
            foods.add(new Coord(snakeCoord));
        }
        for (int i = 1; i < diedSnake.getPoints().size(); ++i) {
            snakeCoord.add(diedSnake.getPoints().get(i));
            snakeCoord.comeField(config.getWidth(), config.getHeight());
            if (generateRandomDouble(0, 1) < config.getDeadFoodProb()) {
                foods.add(new Coord(snakeCoord));
            }
        }
    }

    private void generateFood() {
        HashSet<Coord> freeCells = getFreeCells();
        int countNewFood = config.getFoodStatic() + (int)Math.ceil(config.getFoodPerPlayer() * getAliveSnakesCount()) - foods.size();
        countNewFood = Math.min(countNewFood, freeCells.size());
        Object[] freeCellsArray = freeCells.toArray();

        for (int i = 0; i < countNewFood; ++i) {
            int indexNewFood = generateRandomInt(0, freeCellsArray.length);
            foods.add((Coord) freeCellsArray[indexNewFood]);
            freeCells.remove(freeCellsArray[indexNewFood]);
            freeCellsArray = freeCells.toArray();
        }
    }

    private int getAliveSnakesCount() {
        int result = 0;
        for (Snake snake : snakes) {
            if (snake.isAlive()) {
                ++result;
            }
        }
        return result;
    }

    public Player addPlayer(String name, String ip, int port, NodeRole role) {
        lock.writeLock().lock();
        try {
            HashSet<Coord> freeWindow = getFreeWindow5x5();
            if (freeWindow == null) {
                return null;
            }
            Coord head = getCenterWindow5x5(freeWindow);
            ArrayList<Coord> shakeCoord = new ArrayList<>(2);
            shakeCoord.add(head);
            shakeCoord.add(new Coord(-1, 0));
            Snake playerSnake = new Snake(snakes.size(), shakeCoord, SnakesProto.Direction.RIGHT, config, SnakesProto.GameState.Snake.SnakeState.ALIVE);
            Player newPlayer = new Player(name, getNewId(), ip, port, role);
            snakes.add(playerSnake);
            players.put(newPlayer.getId(), newPlayer);
            return newPlayer;
        } finally {
            lock.writeLock().unlock();
        }
    }

    private int getNewId() {
        int newId = -1;
        for (Player player : players.values()) {
            if (player.getId() >= newId) {
                newId = player.getId();
            }
        }
        for (Snake snake : snakes) {
            if (snake.getPlayerId() >= newId) {
                newId = snake.getPlayerId();
            }
        }
        return newId + 1;
    }

    public Coord getCenterWindow5x5(HashSet<Coord> window) {
        int sumX = 0, sumY = 0;
        for (Coord cell : window) {
            sumX += cell.getX();
            sumY += cell.getY();
        }
        return new Coord(sumX / 5, sumY / 5);
    }

    public NodeRole getMyRole() {
        return Objects.requireNonNull(players.get(myId)).getNodeRole();
    }

    public NodeRole getRole(int id) {
        try {
            return Objects.requireNonNull(players.get(id)).getNodeRole();
        } catch (NullPointerException ex) {
            System.out.println(players);
            return null;
        }
    }

    public Player getDeputy() {
        for (Player player : players.values()) {
            if (player.getNodeRole() == NodeRole.DEPUTY) {
                return player;
            }
        }
        return null;
    }

    private HashSet<Coord> getFreeWindow5x5() {
        HashSet<Coord> freeCells = getFreeCells();
        HashSet<Coord> window = getWindow5x5LeftTop();
        for (int i = 0; i < config.getHeight(); ++i) {
            for (int j = 0; j < config.getWidth(); ++j) {
                if (freeCells.containsAll(window)) {
                    return window;
                }
                moveRightWindow(window);
            }
            moveDownWindow(window);
        }
        return null;
    }

    private HashSet<Coord> getWindow5x5LeftTop() {
        HashSet<Coord> result = new HashSet<>(25);
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                result.add(new Coord(i, j));
            }
        }
        return result;
    }

    private void moveRightWindow (HashSet<Coord> window) {
        for (Coord cell : window) {
            cell.incrementX();
            cell.comeField(config.getWidth(), config.getHeight());
        }
    }

    private void moveDownWindow (HashSet<Coord> window) {
        for (Coord cell : window) {
            cell.incrementY();
            cell.comeField(config.getWidth(), config.getHeight());
        }
    }

    private HashSet<Coord> getFreeCells() {
        HashSet<Coord> result = new HashSet<>();
        for (int i = 0; i < config.getWidth(); ++i) {
            for (int j = 0; j < config.getHeight(); ++j) {
                result.add(new Coord(i, j));
            }
        }
        for (Snake snake : snakes) {
            Coord snakeCoord = new Coord(snake.getHead());
            result.remove(snakeCoord);
            for (int i = 1; i < snake.getPoints().size(); ++i) {
                snakeCoord.add(snake.getPoints().get(i));
                snakeCoord.comeField(config.getWidth(), config.getHeight());
                result.remove(snakeCoord);
            }
        }
        result.removeAll(foods);
        return result;
    }

    private static double generateRandomDouble(double min, double max) {
        return ThreadLocalRandom.current().nextDouble(min, max);
    }

    private int generateRandomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }
}
