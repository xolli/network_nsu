package ru.nsu.fit.supersnake.game.network;

import java.util.ArrayList;

import ru.nsu.fit.supersnake.game.Controller;
import ru.nsu.fit.supersnake.game.StoppedRunnable;
import ru.nsu.fit.supersnake.game.message.MessageSnakesProto;

public class SteerMsgHolder extends StoppedRunnable {
    private final ArrayList<MessageSnakesProto> queue = new ArrayList<>();
    private final Controller controller;
    private final MapSenderId mapSenderId;

    public SteerMsgHolder (Controller controller, MapSenderId mapSenderId) {
        super();
        this.controller = controller;
        this.mapSenderId = mapSenderId;
    }

    @Override
    public void run() {
        try {
            lock.lock();
            while (!stopped.get()) {
                for (MessageSnakesProto message : queue) {
                    if (mapSenderId.getId(message.getIp(), message.getPort()) != -1) {
                        controller.rotatePlayer(message.getMessage().getSteer().getDirection(),
                                mapSenderId.getId(message.getIp(), message.getPort()));
                    }
                }
                queue.clear();
                newEvent.await();
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void hold(MessageSnakesProto message) {
        lock.lock();
        queue.add(message);
        newEvent.signalAll();
        lock.unlock();
    }
}
