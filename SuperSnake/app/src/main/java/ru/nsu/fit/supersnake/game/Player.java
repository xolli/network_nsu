package ru.nsu.fit.supersnake.game;

public class Player {
    private String name;
    private int id;
    private String ip;
    private int port;
    private NodeRole nodeRole;
    private int score;

    public Player(String name, int id, String ip, int port, NodeRole nodeRole) {
        this.name = name;
        this.id = id;
        this.ip = ip;
        this.port= port;
        this.nodeRole = nodeRole;
        this.score = 0;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public NodeRole getNodeRole() {
        return nodeRole;
    }

    public void setNodeRole(NodeRole newNodeRole) {
        nodeRole = newNodeRole;
    }

    public int getScore() {
        return score;
    }

    public void addPoint() {
        ++score;
    }

    public void setIp(String newIp) {
        ip = newIp;
    }

    @Override
    public String toString() {
        return name + " (" + ip + ":" + port + ", " + nodeRole + ", " + score + ")";
    }

    public void setScore(int score) {
        this.score = score;
    }
}
