package ru.nsu.fit.supersnake.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import java.util.HashMap;

import ru.nsu.fit.supersnake.game.Controller;
import ru.nsu.fit.supersnake.game.Field;
import ru.nsu.fit.supersnake.game.network.ListenerLivingNode;
import ru.nsu.fit.supersnake.game.network.Pinger;
import ru.nsu.fit.supersnake.game.network.Resender;

public class JoinedGameActivity extends AppCompatActivity {
    // game specific
    private Field field;

    // Thread and runnable
    private Thread drawerThread;
    private Thread pingerThread;
    private Thread resenderThread;
    private Thread listenerLivingNodeThread;
    private Drawer drawer;
    private Pinger pinger;
    private Resender resender;
    private ListenerLivingNode listenerLivingNode;

    // graphics
    private GameView gameView;
    private Controller controller;

    @SuppressLint("ShowToast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        field = Field.getEmptyInstance();
        controller = Controller.getInstance();
        try {
            gameView = new GameView(this, field, field.getConfig(), controller);
        } catch (InterruptedException ex) {
            Toast.makeText(this, ex.getLocalizedMessage(), Toast.LENGTH_SHORT);
            return;
        }
        initThreads();
        setContentView(gameView);
    }

    private void initThreads() {
        drawer = new Drawer(gameView, field);
        drawerThread = new Thread(drawer);
        drawerThread.start();
        pinger = new Pinger(field, getUsername(), controller.getSender());
        pingerThread = new Thread(pinger);
        pingerThread.start();
        resender = new Resender(controller.getSender(), field);
        resenderThread = new Thread(resender);
        resenderThread.start();
        listenerLivingNode = new ListenerLivingNode(field, controller.getSender(), controller);
        listenerLivingNodeThread = new Thread(listenerLivingNode);
        listenerLivingNodeThread.start();
    }

    private void stopThreads() {
        if (drawerThread != null) {
            drawerThread.interrupt();
        }
        if (pingerThread != null) {
            pingerThread.interrupt();
        }
        if (resenderThread != null) {
            resenderThread.interrupt();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopThreads();
    }

    private String getUsername() {
        SharedPreferences options = getApplicationContext().getSharedPreferences(SetOptionsActivity.PREFS_NAME, 0);
        return options.getString("username", "user");
    }
}