package ru.nsu.fit.supersnake.game;

public enum NodeRole {
    NORMAL,
    MASTER,
    DEPUTY,
    VIEWER
}
