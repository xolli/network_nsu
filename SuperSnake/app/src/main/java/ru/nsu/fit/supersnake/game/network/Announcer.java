package ru.nsu.fit.supersnake.game.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

import ru.nsu.fit.supersnake.game.ConfigServer;
import ru.nsu.fit.supersnake.game.Field;
import ru.nsu.fit.supersnake.game.Converter;

public class Announcer implements Runnable {
//    private static final String groupIp = "239.192.0.4";
    private static final String groupIp = "224.0.0.1";
    private static final int portDest = 9192;
    private InetAddress group;
    private MulticastSocket socket;
    private final ConfigServer config;
    private final Field field;

    public Announcer(Field field, ConfigServer config) throws IOException {
        group = InetAddress.getByName(groupIp);
        socket = new MulticastSocket();
        socket.joinGroup(group);
        this.field = field;
        this.config = config;
    }

    @Override
    public void run() {
        try {
            while (true) {
                byte[] message = Converter.getAnnouncementMsg(field, config);
                DatagramPacket packet = new DatagramPacket(message, message.length, group, portDest);
                socket.send(packet);
                Thread.sleep(1000);
            }
        } catch (InterruptedException | IOException ex) {
            ex.printStackTrace();
        } finally {
            socket.close();
        }
    }
}
