package ru.nsu.fit.supersnake.game.message;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class AnnouncementGames {
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final List<AnnouncementMsg> data;

    public AnnouncementGames() {
        data = new ArrayList<>();
    }

    public void addGame(AnnouncementMsg msg) {
        if (data.contains(msg)) {
            return;
        }
        lock.writeLock().lock();
        data.add(msg);
        lock.writeLock().unlock();
    }

    public int size() {
        lock.readLock().lock();
        int result = data.size();
        lock.readLock().unlock();
        return result;
    }

    public AnnouncementMsg getGame(int index) {
        lock.readLock().lock();
        AnnouncementMsg result = data.get(index);
        lock.readLock().unlock();
        return result;
    }

    public void removeGame(AnnouncementMsg removedGame) {
        lock.writeLock().lock();
        data.remove(removedGame);
        lock.writeLock().unlock();
    }
}
