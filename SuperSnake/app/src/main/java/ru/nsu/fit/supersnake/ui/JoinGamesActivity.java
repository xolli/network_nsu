package ru.nsu.fit.supersnake.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import java.net.DatagramSocket;
import java.net.SocketException;

import ru.nsu.fit.supersnake.R;
import ru.nsu.fit.supersnake.game.Controller;
import ru.nsu.fit.supersnake.game.Field;
import ru.nsu.fit.supersnake.game.Converter;
import ru.nsu.fit.supersnake.game.message.AnnouncementGames;
import ru.nsu.fit.supersnake.game.message.AnnouncementMsg;
import ru.nsu.fit.supersnake.game.network.MulticastReceiver;
import ru.nsu.fit.supersnake.game.network.Sender;
import ru.nsu.fit.supersnake.game.network.UnicastReceiver;

public class JoinGamesActivity extends AppCompatActivity {
    // network
    private final int port = 52795;
    private DatagramSocket socket;
    private AnnouncementGames announcementGames;

    // Thread and runnable
    private Thread listenerExistGameThread;
    private Thread senderThread;
    private Thread multicastReceiverThread;
    private Thread unicastReceiverThread;
    private ListenerExistsGames listenerExistsGames;
    private Sender sender;
    private MulticastReceiver multicastReceiver;
    private UnicastReceiver unicastReceiver;

    // graphic
    private GameDataAdapter adapter;

    // game specific
    private Field field;

    @SuppressLint("ShowToast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_game);
        announcementGames = new AnnouncementGames();
        field = Field.getEmptyInstance();
        field.clear();
        initSocket();
        initRecyclerView();
        initThreads();
        Controller.getInstance().setSender(sender);
    }

    private void initSocket() {
        try {
            socket = new DatagramSocket(port);
        } catch (SocketException e) {
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT);
        }
    }

    private void initAdapter() {
        GameDataAdapter.OnGameClickListener onGameClickListener = new GameDataAdapter.OnGameClickListener() {
            @Override
            public void onGameClick(AnnouncementMsg game) {
                sender.sendMessageWithConfirm(Converter.createJoinMessage(Converter.PlayerType.HUMAN,
                        false, getUsername(), game.getMasterIp(), game.getMasterPort()));
                Intent intent = new Intent(JoinGamesActivity.this, JoinedGameActivity.class);
                startActivity(intent);
            }
        };
        adapter = new GameDataAdapter(this, announcementGames, onGameClickListener);
    }

    private void initRecyclerView() {
        initAdapter();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    private void initThreads() {
        sender = new Sender(socket);
        listenerExistsGames = new ListenerExistsGames(adapter, announcementGames, this);
        multicastReceiver = new MulticastReceiver(announcementGames, this, listenerExistsGames);
        unicastReceiver = new UnicastReceiver(socket, field, sender, Controller.getInstance());

        senderThread = new Thread(sender);
        multicastReceiverThread = new Thread(multicastReceiver);
        listenerExistGameThread = new Thread(listenerExistsGames);
        unicastReceiverThread = new Thread(unicastReceiver);

        senderThread.start();
        multicastReceiverThread.start();
        listenerExistGameThread.start();
        unicastReceiverThread.start();
    }

    private void stopThreads() {
        if (multicastReceiver != null) {
            multicastReceiver.stop();
        }
        if (listenerExistsGames != null) {
            listenerExistsGames.stop();
        }
        if (sender != null) {
            sender.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopThreads();
    }

    private String getUsername() {
        SharedPreferences options = getApplicationContext().getSharedPreferences(SetOptionsActivity.PREFS_NAME, 0);
        return options.getString("username", "user");
    }
}