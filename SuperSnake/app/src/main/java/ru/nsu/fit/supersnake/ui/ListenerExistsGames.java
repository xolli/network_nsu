package ru.nsu.fit.supersnake.ui;

import android.app.Activity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import ru.nsu.fit.supersnake.game.message.AnnouncementGames;
import ru.nsu.fit.supersnake.game.message.AnnouncementMsg;

public class ListenerExistsGames implements Runnable {
    private final GameDataAdapter adapter;
    private final AnnouncementGames games;
    private final Activity listExistGameActivity;
    private final Lock lock  = new ReentrantLock();
    private final Condition updateExistGame = lock.newCondition();
    private final AtomicBoolean continueListen = new AtomicBoolean(true);
    private final HashMap<AnnouncementMsg, Long> lastUpdate = new HashMap<>();

    public ListenerExistsGames(GameDataAdapter adapter, AnnouncementGames games,
                               Activity listExistGameActivity) {
        this.adapter = adapter;
        this.games = games;
        this.listExistGameActivity = listExistGameActivity;
    }

    public void newGame(AnnouncementMsg newGame) {
        games.addGame(newGame);
        lastUpdate.put(newGame, currentTime());
        lock.lock();
        updateExistGame.signal();
        lock.unlock();
    }

    public void notifyAdapter() {
        listExistGameActivity.runOnUiThread(adapter::notifyDataSetChanged);
    }

    @Override
    public void run() {
        lock.lock();
        try {
            while(continueListen.get()) {
                updateExistGame.await(1, TimeUnit.SECONDS);
                notifyAdapter();
                HashSet<AnnouncementMsg> deletingGames = new HashSet<>();
                for (AnnouncementMsg game : lastUpdate.keySet()) {
                    if (currentTime() - lastUpdate.get(game) >= 3500) {
                        deletingGames.add(game);
                    }
                }
                for (AnnouncementMsg deletedGame : deletingGames) {
                    games.removeGame(deletedGame);
                    lastUpdate.remove(deletedGame);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void stop() {
        continueListen.set(false);
    }

    private long currentTime() {
        return System.currentTimeMillis();
    }
}
