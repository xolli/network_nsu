package ru.nsu.fit.supersnake.game.network;

import java.util.ArrayList;

import ru.nsu.fit.supersnake.game.Controller;
import ru.nsu.fit.supersnake.game.Field;
import ru.nsu.fit.supersnake.game.StoppedRunnable;
import ru.nsu.fit.supersnake.game.message.MessageSnakesProto;

public class StateMsgHolder extends StoppedRunnable {
    private final Field field;
    private final ArrayList<MessageSnakesProto> queue = new ArrayList<>();
    private final Sender sender;

    public StateMsgHolder (Field field, Sender sender) {
        super();
        this.field = field;
        this.sender = sender;
    }

    @Override
    public void run() {
        try {
            lock.lock();
            while (!stopped.get()) {
                for (MessageSnakesProto message : queue) {
                    if (message.getMessage().getState().getState().getStateOrder() >= field.getStateOrder()) {
                        Controller.getInstance().setDestination(message.getIp(), message.getPort());
                        field.update(message);
                        field.setMasterIp(message.getIp());
                    }
                }
                queue.clear();
                newEvent.await();
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void hold(MessageSnakesProto message) {
        lock.lock();
        queue.add(message);
        newEvent.signalAll();
        lock.unlock();
    }
}
