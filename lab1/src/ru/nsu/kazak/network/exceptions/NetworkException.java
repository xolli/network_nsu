package ru.nsu.kazak.network.exceptions;

public class NetworkException extends Exception {
    public NetworkException(Exception exception) {
        super(exception);
    }
    public NetworkException() {
        super("Network error");
    }
}
