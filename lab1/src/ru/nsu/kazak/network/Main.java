package ru.nsu.kazak.network;

import java.io.*;
import java.net.*;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Thread.sleep;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        String ipGroup = in.nextLine();
        in.close();
        String msg = "qwe123";
        int portDest = 7000;
        Thread receiver = new MulticastReceiver(ipGroup, portDest);
        receiver.start();
        MulticastPublisher sender = new MulticastPublisher(ThreadLocalRandom.current().nextInt(10000, 60001), ipGroup, portDest);
        sender.run();
    }
}
