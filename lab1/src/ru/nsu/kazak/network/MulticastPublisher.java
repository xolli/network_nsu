package ru.nsu.kazak.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import static java.lang.Thread.sleep;

public class MulticastPublisher extends Thread {
    private DatagramSocket socket;
    private InetAddress groupAddress;
    private byte[] buf;
    private String groupIp;
    private int portDest;

    MulticastPublisher(int myPort, String groupIp, int portDestination) throws SocketException {
        this.socket = new DatagramSocket();
        this.groupIp = groupIp;
        this.portDest = portDestination;
    }

    @Override
    public void run() {
        try {
            groupAddress = InetAddress.getByName(groupIp);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        String multicastMessage = "qwe123";
        buf = multicastMessage.getBytes();
        try {
            while (true) {
                DatagramPacket packet = new DatagramPacket(buf, buf.length, groupAddress, portDest);
                socket.send(packet);
                sleep(1000);
            }
        } catch (InterruptedException | IOException ex) {
            ex.printStackTrace();
            socket.close();
        }
    }
}

// лучше runnable, сокет передавать в коснтруктор