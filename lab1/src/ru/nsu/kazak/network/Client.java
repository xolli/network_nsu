package ru.nsu.kazak.network;

public class Client {
    private String ip;
    private int port;

    Client(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public int hashCode() {
        return port + ip.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (this.getClass() != obj.getClass())
            return false;
        Client otherClient = (Client) obj;
        return this.hashCode() == otherClient.hashCode() ||
                (this.port == otherClient.port && this.ip.equals(otherClient.ip));
    }
}
