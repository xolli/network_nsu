package ru.nsu.kazak.network;

import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public class ControllerClients extends Thread {
    private HashSet<Client> clients;
    private HashMap<Client, Long> times;

    public ControllerClients() {
        clients = new HashSet<Client>();
        times = new HashMap<Client, Long>();
    }

    public void updateAddClient(Client client) {
        synchronized (clients) {
            clients.add(client);
        }
        if (times.containsKey(client)) {
            times.remove(client);
        }
        times.put(client, Instant.now().getEpochSecond());
    }

    @Override
    public void run() {
        while (true) {
            try {
                sleep(5000);
                long now = java.time.Instant.now().getEpochSecond();
                for (Iterator<Client> it = clients.iterator(); it.hasNext(); ) {
                    Client currClient = it.next();
                    if (now - times.get(currClient) >= 5) {
                        synchronized (clients) {
                            it.remove();
                        }
                        times.remove(currClient);
                    }
                }
                printClients();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    private void printClients() {
        System.out.println("---");
        if (clients.isEmpty()) {
            System.out.println("No clients");
        }
        for (Client client : clients) {
            System.out.println(client.getIp() + ":" + client.getPort());
        }
    }
}
