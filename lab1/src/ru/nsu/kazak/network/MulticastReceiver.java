package ru.nsu.kazak.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.HashSet;
import ru.nsu.kazak.network.*;

public class MulticastReceiver extends Thread {
    protected MulticastSocket socket = null;
    protected byte[] buf = new byte[256];
    private String groupAddress;
    private int port;

    MulticastReceiver(String groupAddress, int port) {
        this.groupAddress = groupAddress;
        this.port = port;
    }

    public void run() {
        try {
            socket = new MulticastSocket(port);
            InetAddress group = InetAddress.getByName(groupAddress);
            socket.joinGroup(group);
            ControllerClients controller = new ControllerClients();
            controller.start();
            while (true) {
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet);
                String received = new String(packet.getData(), 0, packet.getLength());
                if ("qwe123".equals(received)) {
                    Client client = new Client(packet.getAddress().toString(), packet.getPort());
                    controller.updateAddClient(client);
                }
                if ("end".equals(received)) {
                    break;
                }
            }
            socket.leaveGroup(group);
            socket.close();
        } catch (IOException ex) {
            System.err.println("Network error");
        }
    }
}